<?php

namespace backend\controllers;

use common\models\Location;
use common\models\UserLocation;
use Yii;
use backend\models\User;
use backend\models\UserSearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin']
                    ]
                ]
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $arrayStatus = User::getArrayStatus();
        $arrayRole = User::getArrayRole();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'arrayStatus' => $arrayStatus,
            'arrayRole' => $arrayRole,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => $model->getUserLocations()
        ]);
        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User(['scenario' => 'admin-create']);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->authManager->assign(Yii::$app->authManager->getRole($model->role), $model->id);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->setScenario('admin-update');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->authManager->revokeAll($id);
            Yii::$app->authManager->assign(Yii::$app->authManager->getRole($model->role), $id);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $userId
     * @param $locationId
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionAssignLocation($userId, $locationId)
    {
        $response = [
            'success' => false,
            'msg' => ''
        ];

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $userLocation = new UserLocation();
            $userLocation->userId = $userId;
            $userLocation->locationId = $locationId;
            if ($userLocation->validate()) {
                $userLocation->save(false);
                $response['success'] = true;
            } else $response['msg'] = $userLocation->getErrors();

            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            $response['msg'] = $e->getMessage();
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }

    /**
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function actionRemoveLocation($id)
    {
        $response = [
            'success' => false,
            'msg' => ''
        ];

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = UserLocation::findOne($id);
            if ($model->delete()) {
                $response['success'] = true;
            } else $response['msg'] = $model->getErrors();

            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            $response['msg'] = $e->getMessage();
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }
}
