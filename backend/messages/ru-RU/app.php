<?php
/**
 * @author Ian Kuznetsov <yankuznecov@ya.ru>
 * Date: 30.09.2015
 * Time: 14:10
 */

return [
    'Hello, {name}' => 'Привет, {name}',
    'Online' => 'В сети',
    'Login' => 'Вход',
    'Logout' => 'Выход',
    'Remember Me' => 'Запомнить меня',
    'Home' => 'Главная',
    'Create' => 'Создать',
    'Update' => 'Обновить',
    'Delete' => 'Удалить',
    'User' => 'Пользователь',
    'Users' => 'Пользователи',
    'Role' => 'Роль',
    'System' => 'Настройки',
    'Username' => 'Логин',
    'Password' => 'Пароль',
    'Repassword' => 'Повторить пароль',
    'Email' => 'Эл.почта',
    'Status' => 'Статус',

    'Locations' => 'Локации',
    'Location ID' => 'Локация',
    'Title' => 'Заголовок',
    'Description' => 'Описание',
    'Name' => 'Название',
    'Active' => 'Активен',

    'Location Addresses' => 'Адреса',
    'Address' => 'Адрес',
    'Latitude' => 'Широта',
    'Longitude' => 'Долгота',

    'This record cannot be deleted!' => 'Эта запись не может быть удалена!',
];