<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Location;

/* @var $this yii\web\View */
/* @var $model common\models\LocationAddress */
/* @var $form yii\widgets\ActiveForm */

/** @var \common\models\User $user */
$user = Yii::$app->user->getIdentity();


$arrayUserLocations = (Yii::$app->user->can('admin'))
    ? Location::getArrayLocations()
    : $user->getArrayAssignedLocations();

$this->registerJsFile('http://api-maps.yandex.ru/2.1/?lang=ru_RU&mode=debug', ['position' => \yii\web\View::POS_HEAD,]);
$this->registerJsFile(Yii::$app->request->getBaseUrl() . '/js/map.js', [
    'depends' => ['yii\web\YiiAsset',]
]);

$latitude = 56.01033650861818;
$longitude = 92.86995867968734;

$latitude = $model->latitude ? $model->latitude : $latitude;
$longitude = $model->longitude ? $model->longitude : $longitude;
?>

<div class="location-address-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'locationId')->dropDownList($arrayUserLocations, ['prompt' => 'Не указано']) ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="row">
        <div class="col-md-9">
            <?= $form->field($model, 'address')->textInput([]) ?>

            <div class="form-group">
                <div id="map" style="min-height: 320px; width: 100%"></div>
            </div>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'latitude')->textInput([]) ?>

            <?= $form->field($model, 'longitude')->textInput([]) ?>

            <div class="alert alert-info">
                Поставьте метку на карте
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>