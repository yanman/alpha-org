<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LocationAddress */

$this->title = Yii::t('app', 'Update') . ': ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Location Addresses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="location-address-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
