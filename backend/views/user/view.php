<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('
    (function(){
        $("#assign-location-btn").on("click", function(){
            var $dropDown = $("#locations");
            $.ajax({
                url: "assign-location",
                data: {locationId: $dropDown.val(), userId: $dropDown.data("user-id")},
                success: function(){
                    $.pjax.reload({container:"#location-grid-pjax"});
                }
            });
            return false;
        });

        var initDeleteEvent = function(){
            $(".delete-location-link").on("click", function(){
                if(confirm("Вы действительно хотите удалить?")){
                    $.ajax({
                        url: "remove-location",
                        data: {id: $(this).data("id")},
                        success: function(){
                            $.pjax.reload({container:"#location-grid-pjax"});
                        }
                    });
                }
                return false;
            });
        };
        initDeleteEvent();

        $("#location-grid-pjax").on("pjax:end", function(){
            initDeleteEvent();
        });
    })();
');
?>
<div class="user-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'email:email',
            [
                'attribute' => 'role',
                'value' => $model->roleLabel,
            ],
            [
                'attribute' => 'status',
                'value' => $model->statusLabel,
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

    <div class="row">
        <div class="col-md-6">

            <div class="form-inline">
                <div class="form-group" style="width: 100%;">
                    <?= \yii\helpers\Html::dropDownList('locations', '', \common\models\Location::getArrayLocations(), [
                        'id' => 'locations',
                        'class' => 'form-control',
                        //'style' => 'width:40%',
                        'data-user-id' => $model->id
                    ]) ?>
                    <?= \yii\bootstrap\Button::widget([
                        'id' => 'assign-location-btn',
                        'label' => 'Добавить',
                        'options' => ['class' => 'btn-primary'],
                    ]) ?>
                </div>
            </div>

            <br>

            <?php \yii\widgets\Pjax::begin([
                'id' => 'location-grid-pjax',
                'timeout' => '2500'
            ]) ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'layout' => '{items}{pager}',
                'columns' => [
                    'location.name',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'template' => '{delete}',
                        'options' => ['width' => '10%'],
                        'contentOptions' => [
                            'style' => 'white-space: nowrap !important;',
                            'class' => 'text-center'
                        ],
                        'buttons' => [
                            'delete' => function ($url, $model) {
                                return \yii\helpers\Html::a('<i class="glyphicon glyphicon-remove"></i>', '#', [
                                    'class' => 'delete-location-link',
                                    'data-id' => $model->id,
                                    'data-toggle' => Yii::t('app', 'Delete'),
                                ]);
                            },
                        ]
                    ],
                ],
            ]); ?>
            <?php \yii\widgets\Pjax::end() ?>
        </div>
    </div>

</div>
