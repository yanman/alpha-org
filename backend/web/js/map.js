/**
 * Created by Yan on 23.09.2015.
 */

ymaps.ready(init);

function init() {
    var searchPlacemark,
        addressMap = new ymaps.Map('map', {
                center: [56.01033650861818, 92.86995867968734],
                zoom: 11,
                controls: ['zoomControl', 'typeSelector', 'fullscreenControl']
            }
        )
        ;

    var searchControl = new ymaps.control.SearchControl({
        options: {
            noPlacemark: true
        }
    });

    addressMap.controls.add(searchControl);

    addressMap.events.add('click', function (e) {
        var coords = e.get('coords');
        if (searchPlacemark) {
            searchPlacemark.geometry.setCoordinates(coords);
        }
        else {
            createPlacemark(coords);
        }
        getAddress(coords);
    });

    function createPlacemark(coords) {
        searchPlacemark = new ymaps.Placemark(coords, {
            iconContent: 'поиск...'
        }, {
            preset: 'islands#grayStretchyIcon',
            draggable: true
        });
        addressMap.geoObjects.add(searchPlacemark);
        searchPlacemark.events.add('dragend', function () {
            getAddress(searchPlacemark.geometry.getCoordinates());
        });
    }

    var $addressField = $("#locationaddress-address"),
        $latitudeField = $("#locationaddress-latitude"),
        $longitudeField = $("#locationaddress-longitude");

    function getAddress(coords) {
        $addressField.val('');
        searchPlacemark.properties.set('iconContent', 'поиск...');
        ymaps.geocode(coords, {
            results: 1
        }).then(function (result) {
                if (result.metaData.geocoder.found == 0) {
                    searchPlacemark.properties.set('iconContent', 'Адрес не найден');
                    return false;
                }
                var o = result.geoObjects.get(0);

                console.log(result.geoObjects.getLength());
                result.geoObjects.each(function (item) {
                    console.log(item);
                });


                var md = o.properties.get('metaDataProperty.GeocoderMetaData');
                var ad = md.AddressDetails;
                updateAddressFields(ad);
                if (md.kind == 'house') {
                    $addressField.val(md.text);
                    searchPlacemark.properties.set({
                        iconContent: o.properties.get('name')
                    });
                } else {
                    ymaps.geocode(coords, {
                        kind: 'locality',
                        results: 1
                    }).then(function (result) {
                            if (result.metaData.geocoder.found == 0) {
                                searchPlacemark.properties.set('iconContent', 'Адрес не найден');
                                return false;
                            }
                            var o = result.geoObjects.get(0);
                            var md = o.properties.get('metaDataProperty.GeocoderMetaData');
                            var ad = md.AddressDetails;
                            updateAddressFields(ad);
                            $addressField.val(md.text);
                            searchPlacemark.properties.set({
                                iconContent: o.properties.get('name')
                            });
                        }
                    );
                }
                $latitudeField.val(coords[0]);
                $longitudeField.val(coords[1]);
            }
        );
    }

    function updateAddressFields(o) {
        if (o.CountryName)
            $("#objectaddress-countryname").val(o.CountryName);
        if (o.AdministrativeAreaName)
            $("#objectaddress-regionname").val(o.AdministrativeAreaName);
        if (o.LocalityName)
            $("#objectaddress-localityname").val(o.LocalityName);
        if (o.ThoroughfareName)
            $("#objectaddress-thoroughfarename").val(o.ThoroughfareName);
        if (o.PremiseNumber)
            $("#objectaddress-premisenumber").val(o.PremiseNumber);
        // recursive stuff
        if (o.Country)
            return updateAddressFields(o.Country);
        if (o.AdministrativeArea)
            return updateAddressFields(o.AdministrativeArea);
        if (o.SubAdministrativeArea)
            return updateAddressFields(o.SubAdministrativeArea);
        if (o.Locality)
            return updateAddressFields(o.Locality);
        if (o.DependentLocality)
            return updateAddressFields(o.DependentLocality);
        if (o.Thoroughfare)
            return updateAddressFields(o.Thoroughfare);
        if (o.Premise)
            return updateAddressFields(o.Premise);
    }


    if ($addressField.val()) {
        createPlacemark([
            $latitudeField.val(),
            $longitudeField.val()
        ]);
        getAddress(searchPlacemark.geometry.getCoordinates());
    }
}