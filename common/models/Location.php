<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "location".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property integer $active
 *
 * @property LocationAddress[] $locationAddresses
 * @property UserLocation[] $userLocations
 */
class Location extends \yii\db\ActiveRecord
{
    const ACTIVE_FALSE = 0;
    const ACTIVE_TRUE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'unique'],
            [['name', 'active'], 'required'],
            [['status', 'active'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
            'active' => Yii::t('app', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocationAddresses()
    {
        return $this->hasMany(LocationAddress::className(), ['locationId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLocations()
    {
        return $this->hasMany(UserLocation::className(), ['locationId' => 'id']);
    }

    /**
     * @return array
     */
    public static function getArrayLocations()
    {
        return ArrayHelper::map(self::find()->where(['active' => self::ACTIVE_TRUE])->all(), 'id', 'name');
    }
}
