<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "location_address".
 *
 * @property integer $id
 * @property integer $locationId
 * @property string $address
 * @property string $latitude
 * @property string $longitude
 * @property string $title
 * @property string $description
 *
 * @property Location $location
 */
class LocationAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['locationId', 'address', 'title', 'description', 'latitude', 'longitude'], 'required'],
            [['locationId'], 'integer'],
            [['address', 'description'], 'string'],
            [['latitude', 'longitude', 'title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'locationId' => Yii::t('app', 'Location ID'),
            'address' => Yii::t('app', 'Address'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id' => 'locationId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLocations()
    {
        return $this->hasMany(UserLocation::className(), ['locationId' => 'locationId']);
    }

    /**
     * @return bool
     */
    public function checkAccessLocation()
    {
        /** @var User $user */
        $user = Yii::$app->user->getIdentity();

        $model = $user->getUserLocations()->andWhere(['locationId' => $this->locationId])->one();
        if ($model !== null) return true;

        return false;
    }
}
