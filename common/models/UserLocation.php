<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_location".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $locationId
 *
 * @property Location $location
 * @property User $user
 */
class UserLocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'locationId'], 'integer'],
            [['locationId'], 'checkAssignment'],
        ];
    }

    /**
     * @param $attribute
     */
    public function checkAssignment($attribute)
    {
        $model = self::find()->where([
            'userId' => $this->userId,
            'locationId' => $this->locationId
        ])->one();
        if ($model instanceof self) {
            $this->addError($attribute, '������ ������� ��� ���������.');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'userId' => Yii::t('app', 'User ID'),
            'locationId' => Yii::t('app', 'Location ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id' => 'locationId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
