<?php
/**
 * @author Alexey Samoylov <alexey.samoylov@gmail.com>
 */
namespace console\controllers;

use common\models\User;
use common\models\UserAccount as Account;
use yii\console\Controller;

class UserController extends Controller
{
    public function actionPassword($username, $password)
    {
        /** @var User $model */
        $model = User::findOne(['username' => $username]);
        if (!$model) {
            $this->stderr("User not found: $username" . PHP_EOL);
            return static::EXIT_CODE_ERROR;
        }
        $model->password_hash = \Yii::$app->security->generatePasswordHash($password);
        $model->updateAttributes(['passwordHash']);
        $this->stdout("OK" . PHP_EOL);
        return static::EXIT_CODE_NORMAL;
    }
}