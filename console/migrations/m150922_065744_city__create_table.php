<?php

use yii\db\Schema;
use yii\db\Migration;

class m150922_065744_city__create_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%location}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'status' => Schema::TYPE_SMALLINT . ' DEFAULT 0',
            'active' => Schema::TYPE_BOOLEAN . ' DEFAULT 1',
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->insert('{{%location}}', [
            'name' => 'Krasnoyarsk',
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%location}}');
    }
}
