<?php

use yii\db\Schema;
use yii\db\Migration;

class m150922_071649_user_location__create_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%user_location}}', [
            'id' => Schema::TYPE_PK,
            'userId' => Schema::TYPE_INTEGER,
            'locationId' => Schema::TYPE_INTEGER,
        ]);

        $this->addForeignKey('FK_user_location_userId', '{{%user_location}}', 'userId',
            '{{%user}}', 'id');
        $this->addForeignKey('FK_user_location_locationId', '{{%user_location}}', 'locationId',
            '{{%location}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('FK_user_location_userId', '{{%user_location}}');
        $this->dropForeignKey('FK_user_location_locationId', '{{%user_location}}');

        $this->dropTable('{{%user_location}}');
    }
}
