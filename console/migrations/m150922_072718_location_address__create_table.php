<?php

use yii\db\Schema;
use yii\db\Migration;

class m150922_072718_location_address__create_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%location_address}}', [
            'id' => Schema::TYPE_PK,
            'locationId' => Schema::TYPE_INTEGER,
            'address' => Schema::TYPE_TEXT,
            'latitude' => Schema::TYPE_STRING,
            'longitude' => Schema::TYPE_STRING,
        ]);

        $this->addForeignKey('FK_location_address_locationId', '{{%location_address}}', 'locationId',
            '{{%location}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('FK_location_address_locationId', '{{%location_address}}');

        $this->dropTable('{{%location_address}}');
    }
}
