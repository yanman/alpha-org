<?php

use yii\db\Schema;
use yii\db\Migration;

class m150923_054127_location_address__add_columns extends Migration
{
    public function up()
    {
        $this->addColumn('{{%location_address}}', 'title', Schema::TYPE_STRING);
        $this->addColumn('{{%location_address}}', 'description', Schema::TYPE_TEXT);
    }

    public function down()
    {
        $this->dropColumn('{{%location_address}}', 'title');
        $this->dropColumn('{{%location_address}}', 'description');
    }
}
