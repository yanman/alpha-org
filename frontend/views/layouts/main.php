<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use common\models\Location;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$this->registerJsFile('http://api-maps.yandex.ru/2.1/?lang=ru_RU&loadByRequire=1', ['position' => \yii\web\View::POS_HEAD,]);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Ищи Альфу</title>
    <?php $this->head() ?>

    <!--[if lt IE 9]>
    <script src="sites/brand2014.alpha.org/themes/alpha2014/js/modernizr.custom.29390.js"></script>
    <![endif]-->

    <!-- HEAD -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="http://www.alpha.org/try"/>
    <link rel="shortcut icon" href="sites/brand2014.alpha.org/themes/alpha2014/favicon.ico"
          type="image/vnd.microsoft.icon"/>
    <meta name="google-site-verification" content="ON4w0XJ4nJJnmSFKanbbfZvIzUrhqgo6ryFXp0Nmwhs"/>
    <meta property="og:site_name" content="Alpha"/>
    <meta name="generator" content="Drupal 7 (http://drupal.org)"/>
    <link rel="canonical" href="/try"/>
    <link rel="shortlink" href="/node/239"/>
    <meta property="og:title" content="Try Alpha"/>
    <meta name="keywords" content="alpha, nicky gumbel, keep searching, meaning of life"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <!-- STYLES -->
    <style type="text/css" media="all">
        @import url("modules/system/system.base.css-ntqozc.css") /*tpa=http://www.alpha.org/modules/system/system.base.css?ntqozc*/;
        @import url("modules/system/system.menus.css-ntqozc.css") /*tpa=http://www.alpha.org/modules/system/system.menus.css?ntqozc*/;
        @import url("modules/system/system.messages.css-ntqozc.css") /*tpa=http://www.alpha.org/modules/system/system.messages.css?ntqozc*/;
        @import url("modules/system/system.theme.css-ntqozc.css") /*tpa=http://www.alpha.org/modules/system/system.theme.css?ntqozc*/;
    </style>
    <style type="text/css" media="all">
        @import url("sites/all/modules/acs_client/css/social-feed.css-ntqozc.css") /*tpa=http://www.alpha.org/sites/all/modules/acs_client/css/social-feed.css?ntqozc*/;
        @import url("modules/aggregator/aggregator.css-ntqozc.css") /*tpa=http://www.alpha.org/modules/aggregator/aggregator.css?ntqozc*/;
        @import url("sites/all/modules/ai_alpha_2014_nav/css/navigation.css-ntqozc.css") /*tpa=http://www.alpha.org/sites/all/modules/ai_alpha_2014_nav/css/navigation.css?ntqozc*/;
        @import url("sites/all/modules/ai_alpha_2014_nav/css/no-theme/jquery-ui-1.9.2.custom.css-ntqozc.css") /*tpa=http://www.alpha.org/sites/all/modules/ai_alpha_2014_nav/css/no-theme/jquery-ui-1.9.2.custom.css?ntqozc*/;
        @import url("sites/brand2014.alpha.org/modules/ai_alpha_redirect/styles/ai_alpha_redirect.css-ntqozc.css") /*tpa=http://www.alpha.org/sites/brand2014.alpha.org/modules/ai_alpha_redirect/styles/ai_alpha_redirect.css?ntqozc*/;
        @import url("modules/field/theme/field.css-ntqozc.css") /*tpa=http://www.alpha.org/modules/field/theme/field.css?ntqozc*/;
        @import url("sites/all/modules/mollom/mollom.css-ntqozc.css") /*tpa=http://www.alpha.org/sites/all/modules/mollom/mollom.css?ntqozc*/;
        @import url("modules/node/node.css-ntqozc.css") /*tpa=http://www.alpha.org/modules/node/node.css?ntqozc*/;
        @import url("sites/all/modules/picture/picture_wysiwyg.css-ntqozc.css") /*tpa=http://www.alpha.org/sites/all/modules/picture/picture_wysiwyg.css?ntqozc*/;
        @import url("modules/user/user.css-ntqozc.css") /*tpa=http://www.alpha.org/modules/user/user.css?ntqozc*/;
        @import url("sites/all/modules/views/css/views.css-ntqozc.css") /*tpa=http://www.alpha.org/sites/all/modules/views/css/views.css?ntqozc*/;
    </style>
    <style type="text/css" media="all">
        @import url("sites/brand2014.alpha.org/modules/ckeditor/ckeditor.css-ntqozc.css") /*tpa=http://www.alpha.org/sites/brand2014.alpha.org/modules/ckeditor/ckeditor.css?ntqozc*/;
        @import url("sites/all/modules/ctools/css/ctools.css-ntqozc.css") /*tpa=http://www.alpha.org/sites/all/modules/ctools/css/ctools.css?ntqozc*/;
        @import url("sites/all/modules/eu-cookie-compliance/css/eu_cookie_compliance.css-ntqozc.css") /*tpa=http://www.alpha.org/sites/all/modules/eu-cookie-compliance/css/eu_cookie_compliance.css?ntqozc*/;
        @import url("sites/all/modules/ai_search/css/ai_search.css-ntqozc.css") /*tpa=http://www.alpha.org/sites/all/modules/ai_search/css/ai_search.css?ntqozc*/;
    </style>
    <style type="text/css" media="all">
        #sliding-popup.sliding-popup-bottom {
            background: #FFF;
        }

        #sliding-popup .popup-content #popup-text h2, #sliding-popup .popup-content #popup-text p {
            color: #000;
        }
    </style>
    <style type="text/css" media="all">
        @import url("sites/brand2014.alpha.org/themes/alpha2014/css/fonts.css-ntqozc.css") /*tpa=http://www.alpha.org/sites/brand2014.alpha.org/themes/alpha2014/css/fonts.css?ntqozc*/;
        @import url("sites/brand2014.alpha.org/themes/alpha2014/css/screen.css-ntqozc.css") /*tpa=http://www.alpha.org/sites/brand2014.alpha.org/themes/alpha2014/css/screen.css?ntqozc*/;
        @import url("sites/brand2014.alpha.org/themes/alpha2014/css/nivo-slider.css-ntqozc.css") /*tpa=http://www.alpha.org/sites/brand2014.alpha.org/themes/alpha2014/css/nivo-slider.css?ntqozc*/;
    </style>
    <!-- SCRIPTS -->
    <script type="text/javascript"
            src="/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="misc/jquery.once.js-v=1.2.js"></script>
    <script type="text/javascript" src="misc/drupal.js-ntqozc.js"></script>
    <script type="text/javascript"
            src="sites/brand2014.alpha.org/modules/ai_alpha_2014/js/ai_alpha_2014.js-ntqozc.js"></script>
    <!--    <script type="text/javascript" src="sites/all/modules/ai_alpha_2014_nav/js/nav.js-ntqozc.js"
                ></script>-->
    <script type="text/javascript" src="sites/all/modules/ai_alpha_2014_nav/js/modernizr.touch.js-ntqozc.js"></script>
    <script type="text/javascript"
            src="sites/all/modules/ai_alpha_2014_nav/js/jquery-ui-1.9.2.custom.min.js-ntqozc.js"></script>
    <!--    <script type="text/javascript"
                src="sites/brand2014.alpha.org/modules/ai_alpha_redirect/scripts/ai_alpha_redirect.js-ntqozc.js"
                ></script>-->
    <script type="text/javascript" src="sites/all/modules/mollom/mollom.js-ntqozc.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/front.js"></script>
    <script type="text/javascript"
            src="sites/brand2014.alpha.org/modules/ai_find_a_course/js/ai_find_a_course.js-ntqozc.js"></script>
    <script type="text/javascript" src="sites/brand2014.alpha.org/themes/alpha2014/js/functions.js-ntqozc.js"></script>
    <!--    <script type="text/javascript" src="sites/brand2014.alpha.org/themes/alpha2014/js/raphael-min.js-ntqozc.js"
                ></script>-->
    <script type="text/javascript"
            src="sites/brand2014.alpha.org/themes/alpha2014/js/jquery.nivo.slider.pack.js-ntqozc.js"></script>
    <script type="text/javascript"
            src="sites/brand2014.alpha.org/themes/alpha2014/js/jquery.dotdotdot.min.js-ntqozc.js"></script>

    <script src="/js/main.js"></script>

    <link rel="stylesheet" href="css/custom.css" media="all">

    <script language="javascript" type="text/javascript">
        function resizeIframe(obj) {
            obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
        }
    </script>
</head>
<body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-239 node-type-try-alpha ">

<div id="container" class="container">
    <div id="container-inner">

        <div id="header">
            <div class="inner">
                <div class="region region-header">
                    <div id="block-ai-alpha-2014-nav-ai-navigation" class="block block-ai-alpha-2014-nav">

                        <div class="content">

                            <div class="navbar" id="global-nav">
                                <div class="main">
                                    <div class="left">
                                        <ul class="complete">
                                            <li>
                                                <a href=""
                                                    >Try</a>
                                            </li>
                                            <li>
                                                <a href=""
                                                    >Run</a>
                                            </li>
                                            <li class="expandable">
                                                <a href=""
                                                    >Journal</a>

                                                <div class="section-sub">
                                                    <ul>
                                                        <li><a href="journal/feature.htm"
                                                                >Feature</a>
                                                        </li>
                                                        <li><a href="journal/stories.htm"
                                                                >Stories</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <a href="javascript:if(confirm(%27http://alpha.org/tv  \n\nThis file was not retrieved by Teleport Pro, because it is addressed on a domain or path outside the boundaries set for its Starting Address.  \n\nDo you want to open it from the server?%27))window.location=%27http://alpha.org/tv%27"
                                                    >TV</a>
                                            </li>
                                        </ul>
                                        <a href="#" class="compressed">Menu</a>
                                    </div>
                                    <div class="center">
                                        <ul>
                                            <li>
                                                <a href="">Альфа</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="right">

                                        <ul>

                                            <li class="search icon">

                                                <div><a class="search-open closed" href="search.htm"
                                                        >Open</a></div>

                                                <div id="search-form-nav">

                                                    <form action="http://www.alpha.org/search/" method="post"
                                                          id="ai-search-form" accept-charset="UTF-8">
                                                        <div>
                                                            <div id="alpha-search-field">
                                                                <div
                                                                    class="form-item form-type-textfield form-item-ai-search-string">
                                                                    <label for="edit-ai-search-string">Search <span
                                                                            class="form-required"
                                                                            title="This field is required.">*</span></label>
                                                                    <input placeholder="Search Alpha" autocomplete="off"
                                                                           class="form-text autocomplete form-text required"
                                                                           type="text" id="edit-ai-search-string"
                                                                           name="ai_search_string" value="" size="60"
                                                                           maxlength="500"/>
                                                                </div>
                                                                <ul id="ais-ac-wrapper"></ul>

                                                                <input type="submit" id="edit-submit" name="op"
                                                                       value="Go" class="form-submit"/></div>
                                                            <input type="hidden" name="form_build_id"
                                                                   value="form-UipKhkHj1wonMrE4jVCERcsBheNrS0-BWQWnHQNik6g"/>
                                                            <input type="hidden" name="form_id" value="ai_search_form"/>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="close">Close</div>

                                            </li>


                                            <li class="complete social icon">
                                                <a href="#">Social</a>

                                                <div class="sub-social">
                                                    <ul>
                                                        <li><a target="_blank"
                                                               href="javascript:if(confirm(%27https://twitter.com/alphacourse  \n\nThis file was not retrieved by Teleport Pro, because it is addressed using an unsupported protocol (e.g., gopher).  \n\nDo you want to open it from the server?%27))window.location=%27https://twitter.com/alphacourse%27"

                                                               class="twitter-nav">Twitter</a></li>
                                                        <li><a target="_blank"
                                                               href="javascript:if(confirm(%27https://www.facebook.com/Alpha  \n\nThis file was not retrieved by Teleport Pro, because it is addressed using an unsupported protocol (e.g., gopher).  \n\nDo you want to open it from the server?%27))window.location=%27https://www.facebook.com/Alpha%27"

                                                               class="facebook-nav">Facebook</a></li>
                                                        <li><a target="_blank"
                                                               href="javascript:if(confirm(%27http://instagram.com/alphacourse  \n\nThis file was not retrieved by Teleport Pro, because it is addressed on a domain or path outside the boundaries set for its Starting Address.  \n\nDo you want to open it from the server?%27))window.location=%27http://instagram.com/alphacourse%27"

                                                               class="instagram-nav">Instagram</a></li>
                                                        <li><a target="_blank"
                                                               href="javascript:if(confirm(%27https://www.youtube.com/user/thealphacourse  \n\nThis file was not retrieved by Teleport Pro, because it is addressed using an unsupported protocol (e.g., gopher).  \n\nDo you want to open it from the server?%27))window.location=%27https://www.youtube.com/user/thealphacourse%27"

                                                               class="youtube-nav">Youtube</a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="complete global icon"><a class="tooltip"
                                                                                title="Connect with Alpha around the world"
                                                                                href=" javascript:if(confirm(%27http://alpha.org/global  \n\nThis file was not retrieved by Teleport Pro, because it is addressed on a domain or path outside the boundaries set for its Starting Address.  \n\nDo you want to open it from the server?%27))window.location=%27http://alpha.org/global%27"
                                                    >Alpha
                                                    Global</a></li>

                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div id="mobile-journal-nav">
                                <div class="mobile-journal-left">
                                    Journal
                                </div>
                                <div class="mobile-journal-right">
                                    <a href="#" class="arrow">239</a>
                                </div>
                            </div>

                            <div id="coverall"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="topcontent" style="display: block;">
            <div class="inner">
                <div class="region region-topcontent">
                    <div id="block-ai-alpha-2014-promo-video" class="block block-ai-alpha-2014">


                        <div class="content">
                            <!-- This is a placeholder theme file, all the actual html is added by front.js in the theme to ensure the video isn't loaded unless required -->
                            <div id="promo_video" class="promo_video_sam" style="width: 1287px; height: 281px;">
                                <a href="#" class="promo_video_close">Close</a>
                                <a href="#" class="try_alpha_button">Попробовать</a>
                                <a href="#" id="playvideo" style="display: none;left: 50$">Play Video</a>
                                <div class="promo_logo">Альфа</div>
                                <div class="promo_text">Альфа - это возможность исследовать жизнь и христианскую веру в дружественной, открытой и неформальной обстановке.</div>
                                <div class="wrapper">
                                    <div class="inner" id="promo_video_wrapper" style="width: 1287px; height: 723.9375px; margin-top: -361.96875px; margin-left: -643.5px;"><div id="videostartframe"></div></div>
                                </div>
                            </div>  </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="body">
            <div class="inner">
                <div id="content">
                    <div class="inner">
                        <div class="tabs">
                        </div>
                        <div class="element-invisible"><a id="main-content"></a></div>
                        <div class="region region-content">
                            <div id="block-system-main" class="block block-system">


                                <div class="content">
                                    <div id="node-239" class="node node-try-alpha node-promoted clearfix">

                                        <div class="main">

                                            <div class="content">


                                                <div id="tryalpha-header">
                                                    <div class="inner">
                                                        <div class="try-landscape">
                                                            <div
                                                                class="field field-name-field-full-width-header field-type-image field-label-hidden">
                                                                <div class="field-items">
                                                                    <div class="field-item even"><img
                                                                            src="img/desktop-1440х442.png"
                                                                            width="1440" height="422"
                                                                            alt="Альфа - это возможность исследовать жизнь и христианскую веру в дружественной открытой и неформальной обстановке"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="try-portrait">
                                                            <div
                                                                class="field field-name-field-full-width-header-mobile field-type-image field-label-hidden">
                                                                <div class="field-items">
                                                                    <div class="field-item even"><img
                                                                            src="/img/mobile.jpg"

                                                                            width="640" height="724"
                                                                            alt="Alpha is an opportunity to explore life and the Christian faith in a friendly, open and informal environment."/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php
                                                $locations = Location::find()->joinWith('locationAddresses', true, 'RIGHT JOIN')->where(['active' => 1])->all();
                                                $arrayLocations = \yii\helpers\ArrayHelper::map($locations, 'id', 'name');
                                                ?>
                                                <?php if (!empty($arrayLocations)): ?>
                                                    <div id="tryalpha-search">
                                                        <div class="inner">
                                                            <div id="find-alpha-block">
                                                                <h3 class="map-icon">Найти Альфу рядом с вами</h3>

                                                                <form action="/" method="post"
                                                                      id="ai-find-a-course-search-form"
                                                                      accept-charset="UTF-8">
                                                                    <div>
                                                                        <div
                                                                            class="form-item form-type-select form-item-country">

                                                                            <?= Html::dropDownList('country', null, $arrayLocations, [
                                                                                'id' => 'tryalpha-country',
                                                                                'class' => 'form-select',
                                                                            ]) ?>
                                                                        </div>

                                                                        <div class="form-actions form-wrapper">
                                                                            <input type="button" id="find-btn" name="op"
                                                                                   value="Найти" class="form-submit"/>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                                <div class="clearboth"></div>

                                                                <div id="map-wrap" class="hide">
                                                                    <div id="map"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <?php endif; ?>

                                                <div class="boxes">
                                                    <div class="boxes-inner">

                                                        <div class="box unit-one">
                                                            <img
                                                                src="img/What_is_Alpha.png"
                                                                alt=""/></div>

                                                        <div class="box unit-two">

                                                            <div class="video-overlay"></div>

                                                            <div class="ratio16-9 try-landscape">
                                                                <div class="video-embed"
                                                                     data-link="https://www.youtube.com/watch?v=ldNyR4gAcm0">
                                                                    <a class="video-play" target="_blank"
                                                                       href=""></a>

                                                                    <div class="video-title">Альфа 2015</div>
                                                                    <img
                                                                        src="img/photofacefun_com_1444408176.jpg"
                                                                        alt=""/>
                                                                </div>
                                                            </div>

                                                            <div class="try-portrait">
                                                                <a class="video-play" target="_blank"
                                                                   href="https://www.youtube.com/watch?v=eSIlA4Wc5Nk"></a>

                                                                <div class="video-title">Альфа 2015</div>
                                                                <img
                                                                    src="img/video-screen-mobile.png"

                                                                    alt=""/>
                                                            </div>

                                                        </div>

                                                        <div class="box unit-one">
                                                            <img
                                                                src="img/Food-360х405.png"
                                                                alt=""/></div>

                                                        <div class="box unit-one">
                                                            <img
                                                                src="img/Talk-360х405.png"

                                                                alt=""/></div>

                                                        <div class="box unit-one">
                                                            <img
                                                                src="img/tryalpha-360kh405.png"
                                                                alt="Альфа проходит в кафе, домах, в клубах по всему миру. Она доступна всем, кому интересно. "/>
                                                        </div>

                                                        <div class="box unit-two">

                                                            <div class="video-overlay"></div>

                                                            <div class="ratio16-9 try-landscape">
                                                                <div class="video-embed"
                                                                     data-link="https://www.youtube.com/watch?v=ldNyR4gAcm0">
                                                                    <a class="video-play" target="_blank"
                                                                       href="#"
                                                                        ></a>

                                                                    <div class="video-title">Sophia's Story</div>
                                                                    <img
                                                                        src="sites/brand2014.alpha.org/files/styles/journal_landscape/public/try-alpha/sophia-land.jpg-itok=Phk_60y2.jpg"

                                                                        alt=""/></div>
                                                            </div>

                                                            <div class="try-portrait">
                                                                <a class="video-play" target="_blank"
                                                                   href="javascript:if(confirm(%27https://www.youtube.com/watch?v=c2-QJ9Tvm8Y  \n\nThis file was not retrieved by Teleport Pro, because it is addressed using an unsupported protocol (e.g., gopher).  \n\nDo you want to open it from the server?%27))window.location=%27https://www.youtube.com/watch?v=c2-QJ9Tvm8Y%27"
                                                                    ></a>

                                                                <div class="video-title">Sophia's Story</div>
                                                                <img
                                                                    src="sites/brand2014.alpha.org/files/styles/journal_landscape/public/try-alpha/sophia-port.jpg-itok=nMmwjoJl.jpg"

                                                                    alt=""/></div>

                                                        </div>

                                                        <div class="box unit-one">
                                                            <img
                                                                src="img/Alpha_stats-360kh405.png"

                                                                alt="27 million people have tried Alpha in 169 countries and 112 languages"/>
                                                        </div>

                                                        <div class="box unit-one">
                                                            <img
                                                                src="img/discuss-360х405.png"

                                                                alt=""/></div>

                                                        <div class="box unit-one">
                                                            <img
                                                                src="img/theguardian-360х405.png"

                                                                alt="‘Альфа дает редкую в наше время возможность обсудить глобальные вопросы в жизни, смерти и их смысла в непринужденной обстановке. Этим она и притягивает тысячи людей по всему миру"/>
                                                        </div>

                                                        <div class="box unit-one">
                                                            <a href="#goinstagram"
                                                               title="Мы в Instagram">
                                                                <img
                                                                    src="img/closer-360х405.png"
                                                                    alt="Рассмотреть поближе"/>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>


                                            </div>
                                            <!-- content -->
                                        </div>
                                        <!-- main -->


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="spacer clearfix"></div>
            </div>
        </div>
        <a name="goinstagram"></a>
        <div id="footer">
            <div class="inner">
                <div class="region region-footer">
                    <div id="block-ai-alpha-2014-nav-ai-footer" class="block block-ai-alpha-2014-nav">

                        <div class="content">
                            <div class="footer-bar">
                                <div class="container">

                                    <p>
                                        &nbsp;
                                    </p>

                                    <!-- Горизонтальная ориентация -->
<!--                                    <iframe src="/inwidget/index.php?width=800&inline=7&view=30&toolbar=false"
                                            scrolling="no" frameborder="no"
                                            style="border:none;width:100%;height:100%;overflow:hidden;"
                                            onload="javascript:resizeIframe(this);"></iframe>-->

                                    <div class="footer-text-area">
                                        <a href="#">&copy; 2015 Alpha</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="block-block-2" class="block block-block">


                        <div class="content">

                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
</div>


<!--<script src="sites/brand2014.alpha.org/themes/alpha2014/js/ga_listeners.js"/>-->
<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] ||
        []).push(function() { try { w.yaCounter32946704 = new Ya.Metrika({ id:32946704, clickmap:true, trackLinks:true,
            accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s =
        d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async
        = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window,
        "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/32946704"
                                                                   style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

</body>
</html>
<?php $this->endPage() ?>
