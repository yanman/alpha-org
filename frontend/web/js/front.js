/**
 * ORDER OF PLAY
 * 1. Detect if browser can autoplay videos
 * 2. Setup the page/video ration using whatRatio()
 * 3. Render the video/landing/content as required using cookes and functionality
 * 4. Setup listeners for al lthe links
 */
(function ($) {
    var videoPaused = false;
    var videoContainer = $('#promo_video');
    var gaCat = 'HomepageVideo';
    var videoDelay = 600; // This is required for the autoplay test to run
    var videoVarSet = false;
    var homepageSwitcherFired = false;

    // Switch the video to play based on a querystring in the URL
    switch (getParameterByName('video')) {
        case 'tom':
            var selectedVideo = 'tom';
            var videoPoster = ' poster="/sites/brand2014.alpha.org/themes/alpha2014/images/alpha_promo_thumbnail_' + selectedVideo + '.jpg"';
            break;
        case 'tori':
            var selectedVideo = 'tori';
            var videoPoster = ' poster="/sites/brand2014.alpha.org/themes/alpha2014/images/alpha_promo_thumbnail_' + selectedVideo + '.jpg"';
            break;
        case 'sophia':
            var selectedVideo = 'sophia';
            var videoPoster = ' poster="/sites/brand2014.alpha.org/themes/alpha2014/images/alpha_promo_thumbnail_' + selectedVideo + '.jpg"';
            break;
        case 'ali':
            var selectedVideo = 'ali';
            var videoPoster = ' poster="/sites/brand2014.alpha.org/themes/alpha2014/images/alpha_promo_thumbnail_' + selectedVideo + '.jpg"';
            break;
        case 'james':
            var selectedVideo = 'james';
            var videoPoster = ' poster="/sites/brand2014.alpha.org/themes/alpha2014/images/alpha_promo_thumbnail_' + selectedVideo + '.jpg"';
            break;
        default:
            var selectedVideo = 'welcome';
            var videoPoster = ' poster="/sites/brand2014.alpha.org/themes/alpha2014/images/alpha_promo_thumbnail_' + selectedVideo + '.jpg"';
            break;
    }

    Drupal.behaviors.ai_alpha_2014_front = {
        attach: function (context, settings) {

            $('#promo_video', context).addClass('promo_video_' + selectedVideo);

            if($("#alpha_promo_video").length > 0) {

            }

            if ($('#block-ai-alpha-2014-promo-video').length > 0) {

                // Get the cookie which shows whether they have seen the video before
                alpha2014Video = $.cookie('alpha2014VideoSam');

                // Set a body class if autoplay is/isnt supported (requires a delay before it can test)
                detect_autoplay(videoDelay);

                setTimeout(function () {

                    var windowWidth = $(window).width();

                    // If cookie exists, show content straight away
                    if (typeof alpha2014Video === 'undefined') {
                        homepage_switcher('video');
                        /*
                         setCookie();
                         */
                    }
                    // Else show the video first and set the cookie
                    else {
                        homepage_switcher('content');
                    }

                    // Setup all the click handlers
                    $('.promo_video_close, .try_alpha_button').on('click', function () {
                        toggleVideoState('pause');
                        homepage_switcher('content');
                        gaTrack(gaCat, 'click', 'Close Video');
                        setCookie();
                        event.preventDefault();
                    });

                    //$('.try_alpha_button').on('click', function () {
                    //    gaTrack(gaCat, 'click', 'Try Alpha Link');
                    //});

                    $('#topcontent').on('click', '#alpha_promo_video', function (event) {
                        toggleVideoState('toggle');
                        gaTrack(gaCat, 'click', 'Pause/UnPause video - by clicking on it');
                        event.preventDefault();
                    });

                    $('#topcontent').on('click', '#playvideo', function (event) {
                        homepage_switcher('video', 'play');
                        $('#playvideo').hide();
                        gaTrack(gaCat, 'click', 'Manual Play - likely due to iOS or Android or that they have seen the video once already');
                        if (windowWidth < 495) {
                            $('.promo_logo, .promo_text').hide();
                        }
                        event.preventDefault();
                    });

                    // If tab is removed from focus, pause the video to improve UX
                    $(document).on('visibilitychange', function (event) {
                        toggleVideoState('pause');
                    });

                }, videoDelay);
            }
        }
    };

    // Switch the content on the homepage on click
    function homepage_switcher(contentType, changeState) {
        changeState = typeof changeState !== 'undefined' ? changeState : 'none';
        videoRegion = jQuery('#topcontent');
        contentElement = jQuery('#body');

        var windowWidth = $(window).width();
        if (windowWidth > 800) {
            if (homepageSwitcherFired == false) {
                selectedVideo += '_hd';
                homepageSwitcherFired = true;
            }
        }

        //video name hard code
        var videoName = 'welcome';
        //video poster hard code
        var videoPoster = ' poster="/img/welcome.png"';
        videoHTML = '<video id="alpha_promo_video"' + videoPoster + ' width="100%" height="100%"> \
                        <source src="/video/' + videoName + '.mp4" /> \
                        <source src=/video/' + videoName + '.webm" type="video/webm" /> \
                     </video>';

        switch (contentType) {
            case 'video':
                // If video HTML is not on the page yet
                if ($('#alpha_promo_video').length == 0) {
                    if ($('#videostartframe').length == 0 && $('body.autoplay-supported').length == 0) {
                        // Check whether autoplay is available, if not, show a start frame instead
                        videoHTML = '<div id="videostartframe"></div>';
                    }
                    // Append the videoHTML to the element
                    videoRegion.find('#promo_video_wrapper').html(videoHTML);
                }
                //videoRegion.show();
                //contentElement.hide();
                // If this is the first time this has successfully run, then set the ended listener too - commented (don't know why this was here)
                // if (videoVarSet == false) {
                //     videoVarSet = true;
                $('#alpha_promo_video').on('ended', function () {
                    homepage_switcher('videolanding');
                });
                // }
                break;
            case 'videolanding':
                videoHTML = '<div id="videostartframe"></div>';
                videoRegion.find('#promo_video_wrapper').html(videoHTML);
                $('#playvideo').show();
                videoRegion.show();
                contentElement.hide();
                break;
            case 'content':
                videoRegion.find('#promo_video_wrapper').html('');
                videoRegion.hide();
                contentElement.show();
                $('#loading-cog-wrapper').remove();
                break;
        }

        // Add tracking to the video when it starts to play
        var theVideo = $('#alpha_promo_video')[0];
        if (theVideo) {
            theVideo.addEventListener('play', function () {
                if (theVideo.currentTime < 0.5) {
                    gaTrack(gaCat, 'video', 'Video starts playing from beginning');
                }
            });
        }

        if (changeState != 'none') {
            toggleVideoState(changeState);

        }
    }

    function setCookie() {
        $.cookie('alpha2014VideoSam', 'true', {
            expires: 30
        });
    }

    function whatRatio() {
        isNarrow = false;
        if ($(window).width() <= 785) {
            isNarrow = true;
        }

        windowWidth = $(window).width();
        windowHeight = $(window).height();
        var browserWidth = windowWidth;
        var browserHeight = windowHeight - 54;

        if (isNarrow) {
            var topMargin = 0;
            var leftMargin = 0;
            videoWidth = 'inherit';
            videoHeight = 'inherit';
        } else {
            // What are we scaling to height or width?
            var widthScale = windowWidth / 16;
            var heightScale = windowHeight / 9;

            if (widthScale > heightScale) {
                videoWidth = windowWidth;
                videoHeight = (windowWidth / 16) * 9;
            } else {
                videoWidth = (windowHeight / 9) * 16;
                videoHeight = windowHeight;
            }

            var imageWidth = windowWidth;
            var imageHeight = (windowWidth * 1.25);

            var topMargin = -(videoHeight / 2);
            var leftMargin = -(videoWidth / 2);
        }

        $("#promo_video .inner").css({
            width: videoWidth,
            height: videoHeight,
            marginTop: topMargin,
            marginLeft: leftMargin
        });
        $("#promo_video").css({
            width: browserWidth,
            height: browserHeight
        });
    }

    function toggleVideoState(chosenState) {
        if ($('#alpha_promo_video').length != 0) {
            var theVideo = $('#alpha_promo_video')[0];
            switch (chosenState) {
                case 'toggle':
                    if (videoPaused) {
                        videoPaused = false;
                        theVideo.play();
                    } else {
                        videoPaused = true;
                        theVideo.pause();
                    }
                    break;
                case 'pause':
                    videoPaused = true;
                    theVideo.pause();
                    break;
                case 'play':
                    videoPaused = false;
                    theVideo.load();
                    theVideo.play();
                    break;
            }
        }
    }

    function gaTrack(gaCat, gaLab, gaVal) {
        //_gaq.push(['_trackEvent', gaCat, gaLab, gaVal + ' [' + selectedVideo + ']']);
        /*      ga("send", {
         "hitType": "event",
         "eventCategory": gaCat,
         "eventLabel": gaVal + ' [' + selectedVideo + ']',
         "eventAction": gaLab,
         "nonInteraction": 1
         });*/
    }

    function detect_autoplay(acceptable_delay) {

        autoplay = false;

        var autoplay_test_content = document.createElement('video');

        //create mp4 and webm sources, 5s long
        var mp4 = document.createElement('source');
        mp4.src = "data:video/mp4;base64,AAAAFGZ0eXBNU05WAAACAE1TTlYAAAOUbW9vdgAAAGxtdmhkAAAAAM9ghv7PYIb+AAACWAAACu8AAQAAAQAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAnh0cmFrAAAAXHRraGQAAAAHz2CG/s9ghv4AAAABAAAAAAAACu8AAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAAFAAAAA4AAAAAAHgbWRpYQAAACBtZGhkAAAAAM9ghv7PYIb+AAALuAAANq8AAAAAAAAAIWhkbHIAAAAAbWhscnZpZGVBVlMgAAAAAAABAB4AAAABl21pbmYAAAAUdm1oZAAAAAAAAAAAAAAAAAAAACRkaW5mAAAAHGRyZWYAAAAAAAAAAQAAAAx1cmwgAAAAAQAAAVdzdGJsAAAAp3N0c2QAAAAAAAAAAQAAAJdhdmMxAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAFAAOABIAAAASAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGP//AAAAEmNvbHJuY2xjAAEAAQABAAAAL2F2Y0MBTUAz/+EAGGdNQDOadCk/LgIgAAADACAAAAMA0eMGVAEABGjuPIAAAAAYc3R0cwAAAAAAAAABAAAADgAAA+gAAAAUc3RzcwAAAAAAAAABAAAAAQAAABxzdHNjAAAAAAAAAAEAAAABAAAADgAAAAEAAABMc3RzegAAAAAAAAAAAAAADgAAAE8AAAAOAAAADQAAAA0AAAANAAAADQAAAA0AAAANAAAADQAAAA0AAAANAAAADQAAAA4AAAAOAAAAFHN0Y28AAAAAAAAAAQAAA7AAAAA0dXVpZFVTTVQh0k/Ou4hpXPrJx0AAAAAcTVREVAABABIAAAAKVcQAAAAAAAEAAAAAAAAAqHV1aWRVU01UIdJPzruIaVz6ycdAAAAAkE1URFQABAAMAAAAC1XEAAACHAAeAAAABBXHAAEAQQBWAFMAIABNAGUAZABpAGEAAAAqAAAAASoOAAEAZABlAHQAZQBjAHQAXwBhAHUAdABvAHAAbABhAHkAAAAyAAAAA1XEAAEAMgAwADAANQBtAGUALwAwADcALwAwADYAMAA2ACAAMwA6ADUAOgAwAAABA21kYXQAAAAYZ01AM5p0KT8uAiAAAAMAIAAAAwDR4wZUAAAABGjuPIAAAAAnZYiAIAAR//eBLT+oL1eA2Nlb/edvwWZflzEVLlhlXtJvSAEGRA3ZAAAACkGaAQCyJ/8AFBAAAAAJQZoCATP/AOmBAAAACUGaAwGz/wDpgAAAAAlBmgQCM/8A6YEAAAAJQZoFArP/AOmBAAAACUGaBgMz/wDpgQAAAAlBmgcDs/8A6YEAAAAJQZoIBDP/AOmAAAAACUGaCQSz/wDpgAAAAAlBmgoFM/8A6YEAAAAJQZoLBbP/AOmAAAAACkGaDAYyJ/8AFBAAAAAKQZoNBrIv/4cMeQ==";

        var webm = document.createElement('source');
        webm.src = "data:video/webm;base64,GkXfo49CgoR3ZWJtQoeBAUKFgQEYU4BnAQAAAAAAF60RTZt0vE27jFOrhBVJqWZTrIIQA027jFOrhBZUrmtTrIIQbE27jFOrhBFNm3RTrIIXmU27jFOrhBxTu2tTrIIWs+xPvwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFUmpZuQq17GDD0JATYCjbGliZWJtbCB2MC43LjcgKyBsaWJtYXRyb3NrYSB2MC44LjFXQY9BVlNNYXRyb3NrYUZpbGVEiYRFnEAARGGIBc2Lz1QNtgBzpJCy3XZ0KNuKNZS4+fDpFxzUFlSua9iu1teBAXPFhL4G+bmDgQG5gQGIgQFVqoEAnIEAbeeBASMxT4Q/gAAAVe6BAIaFVl9WUDiqgQEj44OEE95DVSK1nIN1bmTgkbCBULqBPJqBAFSwgVBUuoE87EQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB9DtnVB4eeBAKC4obaBAAAAkAMAnQEqUAA8AABHCIWFiIWEiAICAAamYnoOC6cfJa8f5Zvda4D+/7YOf//nNefQYACgnKGWgQFNANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQKbANEBAAEQEAAYABhYL/QACIhgAPuC/rKgnKGWgQPoANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQU1ANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQaDANEBAAEQEAAYABhYL/QACIhgAPuC/rKgnKGWgQfQANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQkdANEBAAEQEBRgAGFgv9AAIiGAAPuC/rOgnKGWgQprANEBAAEQEAAYABhYL/QACIhgAPuC/rKgnKGWgQu4ANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQ0FANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQ5TANEBAAEQEAAYABhYL/QACIhgAPuC/rKgnKGWgQ+gANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgRDtANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgRI7ANEBAAEQEAAYABhYL/QACIhgAPuC/rIcU7trQOC7jLOBALeH94EB8YIUzLuNs4IBTbeH94EB8YIUzLuNs4ICm7eH94EB8YIUzLuNs4ID6LeH94EB8YIUzLuNs4IFNbeH94EB8YIUzLuNs4IGg7eH94EB8YIUzLuNs4IH0LeH94EB8YIUzLuNs4IJHbeH94EB8YIUzLuNs4IKa7eH94EB8YIUzLuNs4ILuLeH94EB8YIUzLuNs4INBbeH94EB8YIUzLuNs4IOU7eH94EB8YIUzLuNs4IPoLeH94EB8YIUzLuNs4IQ7beH94EB8YIUzLuNs4ISO7eH94EB8YIUzBFNm3SPTbuMU6uEH0O2dVOsghTM";

        //append sources to test video 
        autoplay_test_content.appendChild(webm);
        autoplay_test_content.appendChild(mp4);

        //set attributes -needs to be visible or IE squawks, so we move it way outside  
        autoplay_test_content.id = "base64_test_video";
        autoplay_test_content.autoplay = true;
        autoplay_test_content.style.position = "fixed";
        autoplay_test_content.style.left = "5000px";

        //add to DOM       
        document.getElementsByTagName("body")[0].appendChild(autoplay_test_content);


        var base64_test_video = document.getElementById("base64_test_video");

        //test for autoplay, 100 ms buffer   
        setTimeout(function () {
            if (!base64_test_video.paused) {
                autoplay = true;
            }

            document.getElementsByTagName("body")[0].removeChild(autoplay_test_content);
            if (autoplay) {
                $('body').addClass('autoplay-supported');
            } else {
                $('body').addClass('autoplay-unsupported');
            }

        }, acceptable_delay);

    }

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    $(document).ready(function () {
        if ($('#block-ai-alpha-2014-promo-video').length > 0) {
            // If js is running, quickly hide a couple of things until needed
            $('#topcontent, #body').hide();

            //whatRatio();
            setTimeout(whatRatio, videoDelay * 2);
        }
    });

    // On browser resize, work out width and height of video
    $(window).resize(function () {
        if ($('#block-ai-alpha-2014-promo-video').length > 0) {
            whatRatio();
        }
    });

})(jQuery);