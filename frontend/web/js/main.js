/**
 * Created by Sony on 25.09.2015.
 */


(function ($) {

    var map;

    Drupal.behaviors.custom = {
        attach: function (context, settings) {

            function initMap() {
                var locations = [],
                    $mapWrap = $('#map-wrap'),
                    $dropDown = $('#tryalpha-country');

                if (map === undefined) {
                    map = new ymaps.Map('map', {
                            center: [56.02278829, 92.89742450],
                            zoom: 11,
                            controls: ['zoomControl', 'typeSelector', 'fullscreenControl']
                        }
                    );
                }

                var searchLocations = function () {
                    $.ajax({
                        url: '/site/search-locations',
                        dataType: 'json',
                        data: {id: $dropDown.val()},
                        success: function (response) {
                            locations = response;

                            $mapWrap.fadeIn(1000);

                            map.geoObjects.removeAll();

                            attachGeoObjects();
                        }
                    });
                };

                var attachGeoObjects = function () {
                    var cluster = new ymaps.Clusterer({clusterDisableClickZoom: true});

                    $.each(locations, function () {
                        var self = this;

                        ymaps.geocode([this.latitude, this.longitude], {results: 1}, {results: 100}).then(function (result) {
                            var o = result.geoObjects.get(0);

                            cluster.add(new ymaps.GeoObject({
                                geometry: {type: "Point", coordinates: o.geometry.getCoordinates()},
                                properties: {
                                    balloonContentHeader: (self.title) ? self.title : o.properties.get('name'),
                                    balloonContentBody: [
                                        o.properties.get('name'),
                                        '<p>',
                                        self.description,
                                        '</p>'
                                    ].join('')
                                }
                            }));

                            map.geoObjects.add(cluster);
                            map.setBounds(map.geoObjects.getBounds());

                            if (map.getZoom() > 18)
                                map.setZoom(18);
                        });
                    });
                };

                searchLocations();
            }

            var $findBtn = $('#find-btn');
            $findBtn.on('click', function (event) {
                event.preventDefault();

                ymaps.load(initMap);
            });

        }
    };

})(jQuery);