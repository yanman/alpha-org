(function ($) {

    var sideNavigation = 0;
    var currentPageDropdown = 0;

    Drupal.behaviors.ai_alpha_2014_nav = {
        attach: function(context, settings) {
                $('document').ready(function(){
                  
                  $('a.tooltip').tooltip({ track: true });             
                  
                  $('li.search .search-open').click(function(){
                    
                    windowSize();
                    
                    if (device == 'desktop') {
                      
                      if ($('li.search').hasClass('open')) {
                        return false;
                      } else {
                  
                        $('li.search .search-open').removeClass('closed').addClass('opened');
                        $('#global-nav li.social').fadeOut();
                        $('#global-nav li.global').fadeOut();
                        $('#global-nav li.login').fadeOut();
                        $('#global-nav li.logout').fadeOut();
                        $('li.search #search-form-nav').show();
                        $('li.search .close').fadeIn();
                        
                        setTimeout(function() { $('li.search #edit-ai-search-string').focus(); }, 50);
                  
                        $('li.search').animate({
                            width: '100%'
                        }, 600, function(){
                        }).addClass('open');
              
                        return false;                    
                      }
                      
                    }
                  
                  });
                  
                  $("#ai-search-form").submit(function(e) {
                    
                    var searchString = $('input#edit-ai-search-string').val();
                      
                    if (searchString == '') {
                      e.preventDefault();
                    }
                        
                  });
                  
                  
                  
                    $('li.search .close').click(function(){  
                    
                      if ($('li.search').hasClass('open')) {
                        closeSearch();
                      } else {
                  
                        return false;                    
                      }
                               
                    });
              
                  $('#global-nav .compressed').click(function(){

                    windowSize();
                
                      if ($('#mobile-journal-nav .arrow').hasClass('open') || $('#cc-menu-mobile .arrow').hasClass('open')) {
                        closeJournalNav();
                      }
        
                      if ($(this).hasClass('open')) {
                        closeMainNav(); 
                      }
                      else {
                          // Open
                          $('body').append('<div id="nav-overlay-outer"><div id="nav-overlay"></div></div>');
                          $('#coverall').fadeIn();

                          // Copy menu items
                          $("#global-nav .main .left").clone().appendTo("#nav-overlay");
                          $("#global-nav .main .right .global").clone().appendTo("#nav-overlay ul");
                          $("#global-nav .main .right .login").clone().appendTo("#nav-overlay ul");
                          $("#global-nav .main .right .logout").clone().appendTo("#nav-overlay ul");
                          $("#global-nav .sub-social").clone().appendTo("#nav-overlay");

                          if (device == 'mobile') {
                              $("#nav-overlay").height('auto');
                              winHeight = $("#nav-overlay").outerHeight();
                
                              $("#nav-overlay-outer").css({
                                 width: '100%',
                                 height: winHeight,
                                 position: 'fixed',
                                 top: -winHeight
                              }).addClass('menu-style');
                
                              $("#nav-overlay").css({
                                 width: '100%',
                                 height: '100%',
                                 position: 'fixed',
                              });
                          }
                          else {

                              $("#nav-overlay").css({
                                 width: 280,
                                 height: '100%',
                                 position: 'fixed',
                                 left: -280,
                                 top: 0,
                              }).addClass('menu-style');
                          }

                          if (device == 'mobile') {
                              $("#nav-overlay-outer").animate({
                                  top: 54
                              }, 600, function(){
                              });
                          }
                          else {
                              $("#nav-overlay").animate({
                                  left: 0
                              }, 400, function(){
                                  $("#nav-overlay").css({
                                     height: '100%',
                                     position: 'fixed'
                                  });
                              });
                          }

                          $(this).toggleClass('open');
                
                      }
        
                      return false;
                  });
              
                  $('#mobile-journal-nav .arrow').click(function(){
                          
                    windowSize();
                
                      if ($('#global-nav .compressed').hasClass('open')) {
                        closeMainNav();
                      }
                      
                      if ($(this).hasClass('open')) {
                        closeJournalNav();
                      }
                      else {
                          // Open
                          $('body').append('<div id="journal-overlay-outer"><div id="journal-overlay"></div></div>');
                          $('#coverall').fadeIn();
              
                          // Copy menu items
                          $("#global-nav .main .left .complete .expandable .section-sub ul").clone().appendTo("#journal-overlay");
              
                          $("#journal-overlay").height('auto');
                          winHeight = $("#journal-overlay").outerHeight();
                          
                          $("#journal-overlay-outer").css({
                             width: '100%',
                             height: winHeight,
                             position: 'fixed',
                             top: -winHeight
                          }).addClass('journal-menu-style');
            
                          $("#journal-overlay").css({
                             width: '100%',
                             height: '100%',
                             position: 'fixed',
                          });
          
                          $("#journal-overlay-outer").animate({
                              top: 100
                          }, 600, function(){
                          });
              
                          $(this).toggleClass('open');
                
                      }
                      
                      return false;
                  });
									
                  $('#cc-menu-mobile .arrow').click(function(){
                          
                    windowSize();
                
                      if ($('#global-nav .compressed').hasClass('open')) {
                        closeMainNav();
                      }
                      
                      if ($(this).hasClass('open')) {
                        closeJournalNav();
                      }
                      else {
                          // Open
                          $('body').append('<div id="journal-overlay-outer"><div id="journal-overlay"></div></div>');
                          $('#coverall').fadeIn();
              
                          // Copy menu items
                          $("#cc-menu .right ul").clone().appendTo("#journal-overlay");
              
                          $("#journal-overlay").height('auto');
                          winHeight = $("#journal-overlay").outerHeight();
                          
                          $("#journal-overlay-outer").css({
                             width: '100%',
                             height: winHeight,
                             position: 'fixed',
                             top: -winHeight
                          }).addClass('journal-menu-style');
            
                          $("#journal-overlay").css({
                             width: '100%',
                             height: '100%',
                             position: 'fixed',
                          });
          
                          $("#journal-overlay-outer").animate({
                              top: 100
                          }, 600, function(){
                          });
              
                          $(this).toggleClass('open');
                
                      }
                      
                      return false;
                  });
        
                  $('#coverall').click(function(){
                    if ($('#mobile-journal-nav .arrow').hasClass('open') || $('#cc-menu-mobile .arrow').hasClass('open')) {
                      closeJournalNav();
                    }
                    if ($('#global-nav .compressed').hasClass('open')) {
                      closeMainNav();
                    }
                  });
                  
                  
                  $(window).resize(function() {
                    if ($('#mobile-journal-nav .arrow').hasClass('open') || $('#cc-menu-mobile .arrow').hasClass('open')) {
                      closeJournalNav();
                    }
                    if ($('#global-nav .compressed').hasClass('open')) {
                      closeMainNav();
                    }
                    if ($('li.search').hasClass('open')) {
                      closeSearch();
                    }
                  });
        
            function closeSearch() {
              $('li.search #search-form-nav').hide();
              $('li.search .close').fadeOut();
              $('li.search .search-open').removeClass('opened').addClass('closed');
              
              $('li.search').animate({
                  width: '54px'
              }, 600, function(){
                $('#global-nav li.social').fadeIn();
                $('#global-nav li.global').fadeIn();
                $('#global-nav li.login').fadeIn();
                $('#global-nav li.logout').fadeIn();
              }).removeClass('open');

              return false;  
            }
        
    
            function closeMainNav() {
    
              windowSize();
              $('#coverall').fadeOut();
    
              // Close
              if (device == 'mobile') {
                  winHeight = $("#nav-overlay").outerHeight();

                  $("#nav-overlay-outer").animate({
                      top: -winHeight
                  }, 600, function(){
                      $("#nav-overlay").remove();
                      $("#nav-overlay-outer").remove();
                  });
              }
              else {
                  $("#nav-overlay").css({
                      height: winHeight,
                      position: 'fixed',
                      left: 0,
                      top: 0
                  });

                  $("#nav-overlay").animate({
                      left: -280
                  }, 400, function(){
                      $("#nav-overlay").remove();
                      $("#nav-overlay-outer").remove();
                  });
              }
    
              $('#global-nav a.compressed').toggleClass('open');
    
            } 
        
            function closeJournalNav() {
    
              windowSize();
              $('#coverall').fadeOut();
    
              // Close
              if (device == 'mobile' || device == 'tablet' || device == 'desktop') {
                  winHeight = $("#journal-overlay").outerHeight();

                  $("#journal-overlay-outer").animate({
                      top: -winHeight
                  }, 600, function(){
                      $("#journal-overlay").remove();
                      $("#journal-overlay-outer").remove();
                  });
              }
    
              $('#mobile-journal-nav a.arrow').toggleClass('open');
							$('#cc-menu-mobile a.arrow').toggleClass('open');
    
            }  
    
            function windowSize() {
              winHeight = $(window).height();
              winWidth = $(window).width();
              device = 'mobile';
              
              if (winWidth < 480) {
                device = 'mobile'
              }
              else if (winWidth > 480 && winWidth <= 800) {
                  device = 'tablet'
              } else {
                device = 'desktop'
              }
            }
          });
        }
    }
})(jQuery);