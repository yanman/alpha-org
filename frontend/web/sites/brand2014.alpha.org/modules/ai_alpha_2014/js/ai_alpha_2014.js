(function ($) {

    var full, half, quarter, third, twoThirds, calc, journalCalc, journalLeftCalc, journalRightCalc, aboutHeight;
    var deviceType = 'mobile';
    var frontTileSizes = [];
    var journalTileSizesLeft = [];
    var journalTileSizesRight = [];

    Drupal.behaviors.ai_alpha_2014 = {
        attach: function (context, settings) {

            $(window).load(function () {
                $('#slider').nivoSlider({
                    effect: 'fade',
                    animSpeed: 300,
                    controlNav: false,
                    manualAdvance: true,
                    controlNav: true,
                });
                aboutHeight = $('#block-block-1').height();
                $('#block-block-1').css('height', '150px');
                ellipsise('#block-block-1');
            });

            if ($('body').hasClass('front') || $('body').hasClass('page-catholic-context-home')) {
                loading($('#body'));
                window.onload = function () {
                    $('#loading-cog-wrapper').fadeOut(300);
                }
            }
            whatDevice();
            percentage();
            adjustWindow();
            generalResize('.main-banner', 16, 9);
            generalResize('#video-video', 16, 9);

            $('.twitter .tweet').hide();
            $('.twitter .tweet').first().show();
            var index = 0;
            var count = $('.twitter .tweet').length - 1;
            var speed = 7000;
            twitterRotator();
            alignSpeakerRows();

            // Resize
            $(window).resize(function () {
                whatDevice();
                percentage();
                adjustWindow();
                generalResize('.main-banner', 16, 9);
                generalResize('#video-video', 16, 9);
                alignSpeakerRows();
                closeCCvideo();
            });

            $("#tryalpha-country").change(function (event) {
                if ($("#tryalpha-country").val().length > 2) {
                    window.location = $("#tryalpha-country").val();
                } else {
                }
            });

            // Catholic Context Videos
            $('.endorsee .photo a.video-play', context).click(function (event) {
                event.preventDefault();

                closeCCtext();

                // Figure out clicked values
                // Video Link
                var link = $(this).attr('href');
                link.replace('http://www.youtube.com/watch?v=', 'http://www.youtube.com/embed/');
                link += '?HD=1;rel=0;showinfo=0;controls=1;autoplay=1';
                // Person Name
                var name = $(this).parent().siblings('.info').children('h5').text();
                // Person Title
                var title = $(this).parent().siblings('.info').children('.title').text();

                // Change Video Box Content
                $('#video-iframe').attr('src', link);
                $('.video-container .video-name').text(name);
                $('.video-container .video-title').text(title);

                // Animation Values
                var position = $('.video-container').offset();
                var videoHeight = $('div#video-video').innerHeight();
                var nameHeight = $('div.video-name').innerHeight();
                var titleHeight = $('div.video-title').innerHeight();
                var containerHeight = videoHeight + nameHeight + titleHeight;
                containerHeight = parseInt(containerHeight);

                // Open Video Box (.video-container)
                $('.video-container').animate({
                    height: containerHeight,
                }, 500, function () {
                    $('.video-container').addClass('active');
                    $('body,html').animate({
                        scrollTop: (position.top - 120)
                    }, 500);
                });
                return false;
            });

            $('.video-container .video-close', context).click(function (event) {
                closeCCvideo();
                return false;
            });

            $('.endorsee .tab', context).click(function (event) {
                event.preventDefault();

                closeCCvideo();

                var thisone = $(this).parent().parent();

                // Figure out clicked values
                // Text
                var text = thisone.children('.endorsement').children('.full').html();
                // Person Name
                var name = thisone.children('h5').text();
                // Person Title
                var title = thisone.children('.title').text();

                // Change Text Box Content
                $('.text-container #text-text').html(text);
                $('.text-container .text-name').text(name);
                $('.text-container .text-title').text(title);

                // Animation Values
                var position = $('.text-container').offset();
                var textHeight = $('div#text-text').innerHeight();
                var nameHeight = $('div.text-name').innerHeight();
                var titleHeight = $('div.text-title').innerHeight();
                var containerHeight = textHeight + nameHeight + titleHeight;
                containerHeight = parseInt(containerHeight);

                $('.text-container').animate({
                    height: containerHeight,
                }, 500, function () {
                    $('.text-container').addClass('active');
                    $('body,html').animate({
                        scrollTop: (position.top - 120)
                    }, 500);
                });

                return false;
            });

            $('.text-container .text-close', context).click(function (event) {
                closeCCtext();
                return false;
            });

            $("#block-block-1").click(function () {
                var clicked = $(this);
                var wasActive = clicked.hasClass('active');

                if (!wasActive) {
                    clicked.animate({
                        height: aboutHeight
                    }, 1000, function () {

                        $("#block-block-1").trigger("update");

                        var position = $(this).offset();
                        $('body,html').animate({
                            scrollTop: (position.top - 108)
                        }, 500);
                    });
                    clicked.addClass('active');
                } else {
                    clicked.animate({
                        height: '150px'
                    }, 1000, function () {
                        $("#block-block-1").trigger("update");
                    });
                    clicked.removeClass('active');
                }
                return false;
            });

            // END Catholic Context Videos

            $('#giving-video a', context).click(function () {
                $(this).parent().load("/loadvideo/giving");
                return false;
            });

            function ellipsise(element) {
                $(element).dotdotdot({
                    watch: "window"
                });
            }

            function whatDevice() {
                var winW = $(window).width();
                if (winW <= 480) {
                    deviceType = 'mobile';
                }
                else if (winW > 480 && winW <= 800) {
                    deviceType = 'tablet';
                }
                else {
                    deviceType = 'desktop';
                }
            }

            function adjustWindow() {
                var firstBox = $('#boxes-homepage').attr('data-boxes-layout');

                if (deviceType == 'mobile') {
                    if (firstBox == 1) {
                        frontTileSizes = ['unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one'];
                    }
                    else if (firstBox == 3) {
                        frontTileSizes = ['unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one'];
                    }

                    journalTileSizesLeft = ['unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one'];
                    journalTileSizesRight = ['unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one'];
                }
                else if (deviceType == 'tablet') {
                    if (firstBox == 1) {
                        frontTileSizes = ['unit-one', 'unit-two', 'unit-one', 'unit-two', 'unit-two', 'unit-one', 'unit-one', 'unit-two', 'unit-one', 'unit-two', 'unit-two', 'unit-one'];
                    }
                    else if (firstBox == 3) {
                        frontTileSizes = ['unit-three', 'unit-one', 'unit-two', 'unit-two', 'unit-one', 'unit-one', 'unit-two', 'unit-one', 'unit-two', 'unit-two', 'unit-one'];
                    }

                    journalTileSizesLeft = ['unit-two', 'unit-one', 'unit-one', 'unit-two', 'unit-one', 'unit-one'];
                    journalTileSizesRight = ['unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one', 'unit-one'];
                }
                else {
                    if (firstBox == 1) {
                        frontTileSizes = ['unit-one', 'unit-two', 'unit-one', 'unit-one', 'unit-one', 'unit-two', 'unit-two', 'unit-one', 'unit-one', 'unit-one', 'unit-two', 'unit-one'];
                    }
                    else if (firstBox == 3) {
                        frontTileSizes = ['unit-three', 'unit-one', 'unit-one', 'unit-one', 'unit-two', 'unit-two', 'unit-one', 'unit-one', 'unit-one', 'unit-two', 'unit-one'];
                    }

                    journalTileSizesLeft = ['unit-two', 'unit-one', 'unit-one', 'unit-two', 'unit-one', 'unit-one'];
                    journalTileSizesRight = ['unit-one', 'unit-one', 'unit-two', 'unit-one', 'unit-one', 'unit-two'];
                }

                changeLayout($('#boxes-homepage .box'), 'img', frontTileSizes);
                changeLayout($('#boxes-journal #journal-left .box'), 'img', journalTileSizesLeft);
                changeLayout($('#boxes-journal #journal-right .box'), 'img', journalTileSizesRight);
                imageResize($('.box'));
            }

            function windowSize2() {
                winHeight = $(window).height();
                winWidth = $(window).width();
                device = 'mobile';

                if (winWidth > 650) {
                    device = 'tablet';
                }
            }

            function percentage() {
                var containerW = $('#body').width();

                quarter = Math.floor((containerW / 100) * 25);
                half = Math.ceil(quarter * 2);
                threeQuarters = Math.ceil(quarter * 3);

                third = Math.floor((containerW / 100) * 33.33);
                twoThirds = Math.floor(third + third);

                full = Math.floor(containerW);

                switch (deviceType) {
                    case 'mobile':
                        calc = full;
                        journalCalc = full;
                        journalLeftCalc = full;
                        journalRightCalc = full;
                        break;
                    case 'tablet':
                        calc = third + twoThirds;
                        journalCalc = twoThirds + third;
                        journalLeftCalc = twoThirds;
                        journalRightCalc = third;
                        break;
                    case 'desktop':
                        calc = half + (quarter * 2);
                        journalCalc = half + half;
                        journalLeftCalc = half;
                        journalRightCalc = half;
                        break;
                }
            }

            function imageResize(element) {
                var newHeight = element.height();

                element.each(function () {
                    var e = $(this);

                    var width = 'full';
                    var type = 'one';

                    if (e.hasClass('unit-one')) {
                        var ratioW = 8;
                        var ratioH = 9;
                    }
                    else if (e.hasClass('unit-two')) {
                        var ratioW = 16;
                        var ratioH = 9;
                        type = 'two';
                    }
                    else if (e.hasClass('unit-three')) {
                        var ratioW = 24;
                        var ratioH = 9;
                        type = 'three';
                    }

                    switch (deviceType) {
                        case 'mobile':
                            width = full;
                            break;
                        case 'tablet':
                            if (type === 'one') {
                                width = third;
                            }
                            else if (type === 'two') {
                                width = twoThirds;
                            }
                            else if (type === 'three') {
                                width = full;
                            }
                            break;
                        case 'desktop':
                            if (type === 'one') {
                                width = quarter;
                            }
                            else if (type === 'two') {
                                width = half;
                            }
                            else if (type === 'three') {
                                width = threeQuarters;
                            }
                            break;
                    }

                    if (e.index() === 0) {
                        newHeight = Math.floor(((width / ratioW ) * ratioH));
                    }

                    e.width(width);
                    e.height(newHeight);

                    if (e.index() === element.length - 1) {
                        $('#body > .inner').width(calc);
                    }
                    $('#boxes-journal').width(journalCalc);
                    $('#journal-left').width(journalLeftCalc);
                    $('#journal-right').width(journalRightCalc);
                });

                //verticalCenter('.twitter', '.tweet-container');
            }

            function artworkUrl(type, length, key) {
                var src = null;
                var filename = null;

                if (type == 'article') {
                    src = '../../../../../../d2f5i9x228sgrc.cloudfront.net/acs/news/index.htm'/*tpa=http://d2f5i9x228sgrc.cloudfront.net/acs/news/*/;
                    if (length == 'single') {
                        src += 'portrait/';
                        filename = '../../../../../BOX_760.jpg'/*tpa=http://www.alpha.org/BOX_760.jpg*/;
                    }
                    else if (length == 'double') {
                        src += 'landscape/';
                        filename = '../../../../../LAND_1440.jpg'/*tpa=http://www.alpha.org/LAND_1440.jpg*/;
                    }

                    src += key + filename;
                }
                else if (type == 'advert') {
                    // set src to https://s3-eu-west-1.amazonaws.com/localdistribution/acs/news/
                    src = '../../../../../../d2f5i9x228sgrc.cloudfront.net/acs/news/index.htm'/*tpa=http://d2f5i9x228sgrc.cloudfront.net/acs/news/*/;
                    if (length == 'single') {
                        src += 'portrait/';
                        filename = '../../../../../BOX_760.jpg'/*tpa=http://www.alpha.org/BOX_760.jpg*/;
                    }
                    else if (length == 'double') {
                        src += 'landscape/';
                        filename = '../../../../../LAND_1440.jpg'/*tpa=http://www.alpha.org/LAND_1440.jpg*/;
                    }
                    else if (length == 'triple') {
                        src += 'triple/';
                        filename = '../../../../../large.jpg'/*tpa=http://www.alpha.org/large.jpg*/;
                    }

                    src += key + filename;
                }
                else {
                    src = '../../../../../../d2f5i9x228sgrc.cloudfront.net/acs/news/index.htm'/*tpa=http://d2f5i9x228sgrc.cloudfront.net/acs/news/*/;
                    if (length == 'single') {
                        src += 'portrait/';
                        filename = '../../../../../BOX_760.jpg'/*tpa=http://www.alpha.org/BOX_760.jpg*/;
                    }
                    else if (length == 'double') {
                        src += 'landscape/';
                        filename = '../../../../../LAND_1440.jpg'/*tpa=http://www.alpha.org/LAND_1440.jpg*/;
                    }

                    src += key + filename;
                }

                return src;
            }

            function changeLayout(box, image, layout) {
                box.each(function () {
                    var curIndex = $(this).index();
                    $(this).removeClass('unit-one').removeClass('unit-two').addClass(layout[curIndex]);

                    var landscape = false;
                    var portrait = false;

                    var img = $(this).find(image);
                    var src = img.attr("src");
                    var type = $(this).attr('data-box-type');
                    var artwork_single = img.attr('data-single');
                    var artwork_double = img.attr('data-double');
                    var artwork_triple = img.attr('data-triple');


                    if (layout[curIndex] === 'unit-one' && artwork_single) {
                        var temp_src = artworkUrl(type, 'single', artwork_single);
                        if (temp_src) src = temp_src;
                    }
                    else if (layout[curIndex] === 'unit-two' && artwork_double) {
                        var temp_src = artworkUrl(type, 'double', artwork_double);
                        if (temp_src) src = temp_src;
                    }
                    else if (layout[curIndex] === 'unit-three' && artwork_triple) {
                        var temp_src = artworkUrl(type, 'triple', artwork_triple);
                        if (temp_src) src = temp_src;
                    }
                    img.attr("src", src);
                });

                if (deviceType === 'tablet') {
                    $('.tablet-hide').hide();
                } else {
                    $('.tablet-hide').show();
                }
            }

            function twitterRotator() {
                $('.twitter .tweet').delay(speed).eq(index).fadeOut(function () {
                    if (index === count) {
                        index = -1;
                    }

                    $('.twitter .tweet').eq(index + 1).fadeIn(function () {
                        index++;
                        twitterRotator();
                    });
                });
            }

            function verticalCenter(outer, inner) {
                var outerDiv = $(outer);
                var innerDiv = $(inner);
                var outerHeight = outerDiv.height();

                innerDiv.each(function () {
                    var innerHeight = innerDiv.height();
                    var newInnerPadding = (((outerHeight / 2) - (innerHeight / 2)) - 10);

                    if (outerHeight < innerHeight) {
                        newInnerPadding = 0;
                    }
                    innerDiv.css('padding-top', newInnerPadding + 'px');

                    var newInnerHeight = innerDiv.outerHeight();
                    if (newInnerHeight > outerHeight) {
                        innerDiv.css('padding-top', '0px');
                    }

                });
            }

            function generalResize(element, ratioW, ratioH) {
                var e = $(element);
                e.each(function () {
                    var width = $(this).width();
                    var height = $(this).height();
                    var newHeight = ((width / ratioW ) * ratioH);

                    $(this).height(newHeight);
                });
            }

            function loading(element) {
                var color = '#000';
                var cog = Raphael('loading-cog', 60, 60);
                var large = cog.path("M40,23.411814 L37.7430629,22.6087321 C36.8828653,22.3041148 36.1922841,21.6464184 35.8426665,20.8035286 C35.4895874,19.9571772 35.5155491,19.0035174 35.9084362,18.1744738 L36.9347887,16.0144604 L33.9820781,13.0617498 L31.8220647,14.0898331 C30.9964826,14.4827201 30.0410921,14.506951 29.1947407,14.1573335 C28.35012,13.8059851 27.6941544,13.1154039 27.3895372,12.2534755 L26.5864553,10 L22.411814,10 L21.6087321,12.2534755 C21.3041148,13.1136732 20.6464184,13.8077159 19.8035286,14.1573335 C18.9571772,14.506951 18.0035174,14.4827201 17.1762046,14.0898331 L15.0144604,13.0617498 L12.0634805,16.0144604 L13.0915638,18.1744738 C13.4844509,19.0035174 13.5086818,19.9571772 13.1573335,20.8017978 C12.8059851,21.6464184 12.1171347,22.3041148 11.2552063,22.6087321 L9,23.411814 L9,27.5864553 L11.2552063,28.3878064 C12.1154039,28.6941544 12.8077159,29.3535816 13.1573335,30.1947407 C13.5086818,31.0410921 13.4844509,31.9964826 13.0915638,32.8237954 L12.0634805,34.9838088 L15.0144604,37.9365195 L17.1762046,36.9084362 C18.0017866,36.5155491 18.9589079,36.4913182 19.8035286,36.8409357 C20.6481492,37.1922841 21.3041148,37.8828653 21.6087321,38.7430629 L22.411814,41 L26.5864553,41 L27.3843448,38.7586399 C27.6924237,37.8897884 28.3553124,37.1940149 29.2068561,36.839205 C30.0445536,36.4895874 30.9895595,36.5138183 31.8082184,36.9032438 L33.9820781,37.9382502 L36.9347887,34.9855396 L35.9084362,32.8237954 C35.5138183,31.9982134 35.4895874,31.0445536 35.8409357,30.199933 C36.1922841,29.3535816 36.8828653,28.6958852 37.7430629,28.3895372 L40,27.5864553 L40,23.411814 Z M24.5008654,31.183016 C21.3612305,31.183016 18.816984,28.6370387 18.816984,25.4991346 C18.816984,22.3594997 21.3612305,19.8135224 24.5008654,19.8135224 C27.6387695,19.8135224 30.1847468,22.357769 30.1847468,25.4991346 C30.1847468,28.6370387 27.6387695,31.183016 24.5008654,31.183016 L24.5008654,31.183016 Z M24.5008654,31.183016");
                large.attr("fill", color);
                large.attr("stroke-width", 0);

                var spinLarge = Raphael.animation({transform: "r360"}, 2500).repeat(Infinity);
                large.animate(spinLarge);

                var small = cog.path("M51,42.0804453 L49.8341033,42.4921235 C49.3922722,42.6500735 49.0344393,42.9911783 48.8530029,43.4247007 L48.8530029,43.426381 C48.6715666,43.8615837 48.6866863,44.3555976 48.8882822,44.7807183 L49.4157917,45.8964503 L47.8937421,47.4204999 L46.7715246,46.8878387 C46.348173,46.6845201 45.8593028,46.6744381 45.4292314,46.852552 C44.9890802,47.0373871 44.6463671,47.3952951 44.4884502,47.8439403 L44.0785384,49 L41.9214616,49 L41.5081898,47.8372191 C41.351953,47.3919345 41.0125997,47.0357068 40.5774885,46.8559126 C40.1406972,46.6744381 39.648467,46.6878807 39.2200756,46.8911993 L38.1062579,47.4204999 L36.5842083,45.8964503 L37.1117178,44.7823987 C37.3133137,44.3555976 37.3250735,43.8615837 37.1469971,43.4247007 C36.9655607,42.9911783 36.6077278,42.6500735 36.1658967,42.4921235 L35,42.0804453 L35,39.9245957 L36.1658967,39.5112371 C36.6077278,39.3516068 36.9655607,39.0138626 37.1469971,38.5786599 C37.3267535,38.1400966 37.3133137,37.6477631 37.1117178,37.2226423 L36.5842083,36.1069103 L38.1062579,34.5828607 L39.2200756,35.1121613 C39.648467,35.3154799 40.1406972,35.3272422 40.5774885,35.147448 C41.0125997,34.9659735 41.351953,34.6080655 41.5081898,34.1661416 L41.9214616,33 L44.0785384,33 L44.4918102,34.1661416 C44.648047,34.6097459 44.9874003,34.9659735 45.4225115,35.147448 C45.8593028,35.3289225 46.351533,35.3154799 46.7782444,35.1121613 L47.8937421,34.5828607 L49.4157917,36.1069103 L48.8882822,37.2226423 C48.6866863,37.6494434 48.6732465,38.1400966 48.8530029,38.5786599 C49.0344393,39.0138626 49.3922722,39.3516068 49.8341033,39.5112371 L51,39.9245957 Z M43,43.9355177 C44.6211676,43.9355177 45.9315414,42.6215081 45.9315414,41.0016803 C45.9315414,39.3818526 44.6211676,38.0695232 43,38.0695232 C41.3805124,38.0695232 40.0701386,39.3818526 40.0701386,41.0016803 C40.0701386,42.6215081 41.3805124,43.9355177 43,43.9355177 Z M43,43.9355177");
                small.attr("fill", color);
                small.attr("stroke-width", 0);

                var spinSmall = Raphael.animation({transform: "r-360"}, 1500).repeat(Infinity);
                small.animate(spinSmall);
            }

            function alignSpeakerRows() {
                var height = 0;

                $('.endorsee .info').each(function () {
                    $(this).height('auto');

                    $(this).each(function () {
                        var element_height = $(this).height();
                        if (element_height > height) height = element_height;
                    });
                })
                $('.endorsee .info').each(function () {
                    $(this).height(height);
                })
            }

            function closeCCvideo() {
                if ($('.video-container').hasClass('active')) {

                    // Close Video Box (.video-container)
                    $('.video-container').animate({
                        height: 0,
                    }, 500, function () {
                        $('.video-container').removeClass('active');
                    });

                    $('#video-iframe').attr('src', '#');

                }
                ;
            }

            function closeCCtext() {
                if ($('.text-container').hasClass('active')) {

                    // Close Video Box (.video-container)
                    $('.text-container').animate({
                        height: 0,
                    }, 500, function () {
                        $('.text-container').removeClass('active');
                    });

                }
                ;
            }

        }
    };

})(jQuery);