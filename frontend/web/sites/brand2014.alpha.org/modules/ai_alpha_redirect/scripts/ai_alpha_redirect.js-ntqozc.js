function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

(function ($) {

    Drupal.behaviors.airedirect = {
        attach: function (context, settings) {

            var countries = new Array();
            countries ['US'] = new Array('http://www.alphausa.org/', 'USA', 1, 1);
            countries ['AU'] = new Array('http://www.alphaaustralia.org.au/', 'Australia', 0, 1);
            // countries ['AT'] = new Array('http://www.alphakurs.at/', 'Austria', 0, 1);
            countries ['CA'] = new Array('http://www.alphacanada.org/', 'Canada', 0, 0);
            countries ['CZ'] = new Array('http://www.kurzyalfa.cz/', 'Czech Republic', 1, 1);
            countries ['DK'] = new Array('http://www.alphadanmark.dk/', 'Denmark', 0, 1);
            countries ['FI'] = new Array('http://www.alfakurssi.fi/', 'Finland', 0, 1);
            countries ['FR'] = new Array('http://www.parcoursalpha.fr/', 'France', 0, 1);
            // countries ['DE'] = new Array('http://www.alphakurs.de/', 'Germany', 0, 1);
            countries ['HK'] = new Array('http://www.alpha.org.hk/', 'Hong Kong', 0, 1);
            countries ['CN'] = new Array('http://www.alpha.org.hk/', 'China', 0, 1);
            countries ['TW'] = new Array('http://www.alpha.org.hk/', 'Taiwan', 0, 1);
            countries ['HU'] = new Array('http://alphakurzus.hu/', 'Hungary', 0, 1);
            countries ['IS'] = new Array('http://www.alfa.is/', 'Iceland', 0, 1);
            countries ['IN'] = new Array('http://www.alphaindia.org/', 'India', 0, 1);
            countries ['IE'] = new Array('http://www.alphacourse.ie/', 'Ireland', 0, 1);
            countries ['JP'] = new Array('http://alphajapan.jp/', 'Japan', 0, 1);
            countries ['KR'] = new Array('http://www.alphakorea.org/', 'Korea', 0, 1);
            countries ['AF'] = new Array('http://www.alphakorea.org/', 'Afghanistan', 0, 1);
            countries ['AZ'] = new Array('http://www.alphakorea.org/', 'Azerbaijan', 0, 1);
            countries ['KZ'] = new Array('http://www.alphakorea.org/', 'Kazakhstan', 0, 1);
            countries ['KG'] = new Array('http://www.alphakorea.org/', 'Kyrgyzstan', 0, 1);
            countries ['UZ'] = new Array('http://www.alphakorea.org/', 'Uzbekistan', 0, 1);
            countries ['NL'] = new Array('http://www.alpha-cursus.nl/', 'Netherlands', 0, 1);
            countries ['NZ'] = new Array('http://www.alpha.org.nz/', 'New Zealand', 0, 1);
            countries ['NO'] = new Array('http://www.alpha-kurs.no/', 'Norway', 0, 1);
            countries ['PH'] = new Array('http://www.alphaph.org/', 'Philippines', 1, 1);
            countries ['PL'] = new Array('http://www.alfapolska.org/', 'Poland', 0, 1);
            countries ['SG'] = new Array('http://www.alpha.org.sg/', 'Singapore', 0, 1);
            countries ['BN'] = new Array('http://www.alpha.org.sg/', 'Brunei', 0, 1);
            countries ['KH'] = new Array('http://www.alpha.org.sg/', 'Cambodia', 0, 1);
            countries ['ID'] = new Array('http://www.alpha.org.sg/', 'Indonesia', 0, 1);
            countries ['LA'] = new Array('http://www.alpha.org.sg/', 'Laos', 0, 1);
            countries ['MM'] = new Array('http://www.alpha.org.sg/', 'Myanmar', 0, 1);
            countries ['TH'] = new Array('http://www.alpha.org.sg/', 'Thailand', 0, 1);
            countries ['VN'] = new Array('http://www.alpha.org.sg/', 'Vietnam', 0, 1);
            countries ['ZA'] = new Array('http://www.alphasa.co.za/', 'South Africa', 0, 1);
            countries ['ES'] = new Array('http://cursoalpha.es/', 'Spain', 0, 1);
            countries ['SE'] = new Array('http://www.alphasverige.org/', 'Sweden', 0, 1);
            countries ['UA'] = new Array('http://www.alpha.org.ua/ru/', 'Ukraine', 0, 1);
            countries ['AR'] = new Array('http://www.alphalatinoamerica.org/', 'Argentina', 0, 0);
            countries ['BO'] = new Array('http://www.alphalatinoamerica.org/', 'Bolivia', 0, 0);
            countries ['BR'] = new Array('http://www.alphalatinoamerica.org/', 'Brazil', 0, 0);
            countries ['CL'] = new Array('http://www.alphalatinoamerica.org/', 'Chile', 0, 0);
            countries ['CO'] = new Array('http://www.alphalatinoamerica.org/', 'Colombia', 0, 0);
            countries ['CR'] = new Array('http://www.alphalatinoamerica.org/', 'Costa Rica', 0, 0);
            countries ['SV'] = new Array('http://www.alphalatinoamerica.org/', 'El Salvador', 0, 0);
            countries ['GT'] = new Array('http://www.alphalatinoamerica.org/', 'Guatemala', 0, 0);
            countries ['HN'] = new Array('http://www.alphalatinoamerica.org/', 'Heard Island', 0, 0);
            countries ['MX'] = new Array('http://www.alphalatinoamerica.org/', 'Mexico', 0, 0);
            countries ['NI'] = new Array('http://www.alphalatinoamerica.org/', 'Nicaragua', 0, 0);
            countries ['PA'] = new Array('http://www.alphalatinoamerica.org/', 'Panama', 0, 0);
            countries ['PY'] = new Array('http://www.alphalatinoamerica.org/', 'Paraguay', 0, 0);
            countries ['PE'] = new Array('http://www.alphalatinoamerica.org/', 'Peru', 0, 0);
            countries ['UY'] = new Array('http://www.alphalatinoamerica.org/', 'Uruguay', 0, 0);
            countries ['VE'] = new Array('http://www.alphalatinoamerica.org/', 'Venezuela', 0, 0);
            countries ['CU'] = new Array('http://www.alphalatinoamerica.org/', 'Cuba', 0, 0);
            countries ['EC'] = new Array('http://www.alphalatinoamerica.org/', 'Ecuador', 0, 0);
            countries ['DO'] = new Array('http://www.alphalatinoamerica.org/', 'Dominican Republic', 1, 0);
            countries ['PR'] = new Array('http://www.alphana.org/', 'Puerto Rico', 0, 1);
            countries ['MY'] = new Array('http://www.alpha.org.my/', 'Malaysia', 0, 1);
            countries ['RU'] = new Array('http://www.alphacourse.ru/', 'Russia', 0, 1);
            //   countries ['CH'] = new Array('http://www.alphalive.ch/', 'Switzerland', 0, 1);

            //V1 of country code lookup
            //var req = new XMLHttpRequest();
            //req.open('GET', document.location, false);
            //req.send(null);
            //var countryCode = req.getResponseHeader('X-Country-Code');

            //V2 of country code lookup - allows the system to work with Varnish
            var cookieCountry = readCookie('country_redirect');
            if (cookieCountry == null) {
                var req = new XMLHttpRequest();
                var url = 'get-country';
                req.open('GET', url, false);
                req.send(null);
                var countryCode = req.responseText;
            } else {
                // Cookie found
                if (cookieCountry != 'GB') {
                    // If cookie choice not GB redirect them
                    window.location = countries[cookieCountry][0];
                }
            }

            // Hard redirect some countries
            //            if (countryCode != null && countryCode != '' && countryCode != 'GB' && typeof countries[countryCode] != 'undefined' && countries[countryCode][3] != 1) {
            //                window.location = countries[countryCode][0];
            //            }
            //            else {
            // Show overlay if you come from a country other than GB
  
  //debug with this - countryCode = 'US';
  
           if (countryCode != null && countryCode != '' && countryCode != 'GB' && typeof countries[countryCode] != 'undefined'
               ){

            // Only show overlay on homepage
                if ($('body').hasClass('front')) {

                    // No cookie set, show overlay options
                    if (countries[countryCode][2] == 1) {
                        var extra = Drupal.t('the') + ' ';
                    }
                    else {
                        var extra = '';
                    }
                    
                    var htmlWarning = '<div id="redirect-warning"><div id="redirect-warning-close">x</div>' + 'You are currently viewing the Alpha International website, would you like to visit ' +'<a href="' + countries[countryCode][0] + '" class="goto" >Alpha ' + countries[countryCode][1] + '</a>?</div>';

                    $(htmlWarning).insertAfter("#body .inner");
                    
                    ; 
                }
                


 
            }
        //            }
        
        
        
        //animate
        $('document').ready(function(){
        	$('#redirect-warning').slideDown('slow');
        	
        	$('#redirect-warning-close').click(function(){
        		$('#redirect-warning').slideUp('slow');
        	});
        	
        });
        
        
        
        }
    };

})(jQuery);