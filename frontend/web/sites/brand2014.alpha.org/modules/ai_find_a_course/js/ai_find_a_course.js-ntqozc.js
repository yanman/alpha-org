(function ($) {

  Drupal.behaviors.rc_find_a_course = {
    attach: function (context, settings) {
      $("#fac-country").change(function(event) {
        if ($("#fac-country").val().length>2) {
          window.location = $("#fac-country").val();
        }
      });
    }
  };

}(jQuery));