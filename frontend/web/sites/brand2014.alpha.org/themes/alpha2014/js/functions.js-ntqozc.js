(function ($) {

	//BREAKPOINTS
	var breakpointMobile = 0;
	var breakpointLandscapeMobile = 481;
	var breakpointTabletPortrait = 721; //extra one here so can detect for homepage image
	var breakpointTabletLandscape = 801;
	var breakpointDesktop = 1025;
	var breakpointDesktopLarge = 1241; //TODO NOT DEFINED PROPERLY
	var src;

    Drupal.behaviors.alpha2014 = {
        attach: function(context, settings) {

					// Try Page
					$(window, context).scroll(function() {
						var windscroll = $(window).scrollTop();
						if (windscroll >= 54) {
							$('#tryalpha-search').addClass('mobile-fixed');
						} else {
							$('#tryalpha-search').removeClass('mobile-fixed');
						}
					});

					$(window, context).load(function() {
					    $('.slider-control', context).nivoSlider({
					    	effect: 'fold',
								animSpeed: 300,
								controlNav: false,
								manualAdvance: true,
								controlNav: true,
								afterLoad: function(){
									// YFS Slider on click, image or slice
									$('.yfs-slider .nivoSlider, .yfs-slider .nivoSlider .nivo-slice').on('click', 'img', function(e) {
										
										var token = '';
										
										var wrapper = $(this).parents('.nivoSlider');
										
										// Find main image (which changes its source based on which image is visible)
										var clicked = wrapper.find('.nivo-main-image');
										// Get the current images' src
										var imageRef = clicked.attr('src');
										// Find right image to get token from
										var image = wrapper.children('[src="' + imageRef + '"]');
										// Get relevant youtube link
										var link = image.attr('data-link');
										// Get link token
										token = link.split('watch?v=');
								
										// Build embed code
										var embeddedVideo = '<iframe width="100%" height="100%" src="//www.youtube.com/embed/' + token[1] + '?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';
										
										// Action!
										$('.yfs-slider .nivoSlider').prepend('<div id="yfs-slider-embed" class="ratio16-9">' + embeddedVideo + '</div>');
										videoResize('.ratio16-9');
						        e.preventDefault();
									});
								},
								beforeChange: function(){
									$('.yfs-slider .nivoSlider #yfs-slider-embed').remove();
								}, 
					    });
					    function videoResize(element) {
					        var e = $(element);
					        e.each(function(){
					          var width = $(this).width();
					          var height = $(this).height();
					          var newHeight = ((width / 16 ) * 9);

					          $(this).height(newHeight);
					        });
					    }
					});

			// Invitation 2014 - Homepage
			if ($("#boxes-invitation-2014").length) {
				
				$(window).on("load resize",function(e){
				
					if ($(window).width() <= 815){	
						$(".box.five-ways").removeClass("unit-two").addClass("unit-one");
					}
					else {
						$(".box.five-ways").removeClass("unit-one").addClass("unit-two");
					}
					
					if ($(window).width() <= 815){	
						$(".box.pray").removeClass("unit-two").addClass("unit-one");
					}
					else {
						$(".box.pray").removeClass("unit-one").addClass("unit-two");
					}
					
					if ($(window).width() <= 815){	
						$(".box.twitter").removeClass("unit-one").addClass("unit-two");
					}
					else {
						$(".box.twitter").removeClass("unit-two").addClass("unit-one");
					}
					
				});
				
			}
					
			$(window).unload(function() {
				// Find deleted cookies
				if ($.cookie('checked-item_list01') === 'false') { 
					$.removeCookie('checked-item_list01');
				}
			
				if ($.cookie('checked-item_list02') === 'false') { 
					$.removeCookie('checked-item_list02');
				}

				if ($.cookie('checked-item_list03') === 'false') { 
					$.removeCookie('checked-item_list03');
				}
			
				if ($.cookie('checked-item_list04') === 'false') { 
					$.removeCookie('checked-item_list04');
				}
			
				if ($.cookie('checked-item_list05') === 'false') { 
					$.removeCookie('checked-item_list05');
				}
			
				if ($.cookie('checked-item_list06') === 'false') { 
					$.removeCookie('checked-item_list06');
				}
			
				if ($.cookie('checked-item_list07') === 'false') { 
					$.removeCookie('checked-item_list07');
				}
			
				if ($.cookie('checked-item_list08') === 'false') { 
					$.removeCookie('checked-item_list08');
				}
			
				if ($.cookie('checked-item_list09') === 'false') { 
					$.removeCookie('checked-item_list09');
				}
				
				if ($.cookie('checked-item_list10') === 'false') { 
					$.removeCookie('checked-item_list10');
				}
				
				if ($.cookie('checked-item_list11') === 'false') { 
					$.removeCookie('checked-item_list11');
				}
				
				if ($.cookie('checked-item_list12') === 'false') { 
					$.removeCookie('checked-item_list12');
				}
			});
					
			$(window).load(function() {
			
				// Invitation 2014 - Checklist
				$('input[type="checkbox"]').filter(':checked').each(function() {
					$(this).parent(".inner").addClass("checked");
				});
				
				// Invitation 2014 - FAQs
				var pluses = {};

				//  $('a').on("click", function (e) {
				//       e.preventDefault();
				//   });
				$('#block-views-faqs-block .views-row').each(function(index, element){
				  // Pluses
				  $(element).children('.views-field').children('.field-content').children('.faq-header').append('<div class="change-plus" id="plus-'+index+'"></div>');
				  var plus = Raphael('plus-'+index, 60, 60);
				  pluses[index]  = plus.path("M 27 17 L 33 17 L 33 27 L 43 27 L 43 33 L 33 33 L 33 43 L 27 43 L 27 33 L 17 33 L 17 27 L 27 27Z M 27 17 Z");
				  var color = '#000';
				  pluses[index].attr("fill", color);
				  pluses[index].attr("stroke-width", 0);

				});

				$(".faq-header").click(function(){
				var clicked = $(this).parent().parent().parent();

				var wasActive = clicked.hasClass('active');
				var previouslyActive = $('#block-views-faqs-block .views-row.active');

				// Close any tabs that are open
				if (previouslyActive.length) {
					$(previouslyActive).children('.views-field').children('.field-content').children('.faq-content').animate({
						height: 0
					}, 1000);
					$(previouslyActive).removeClass('active');
					var index = $(previouslyActive).index();
					pluses[index].animate({
						transform: "r" + (0)
						}, 1000);
				}

				// If it wasn't just open, open it
				if (!wasActive) {
					var height = $(clicked).children('.views-field').children('.field-content').children('.faq-content').children('.faq-content-inner').height();

					var index = $(clicked).index();
					pluses[index].animate({
						transform: "r" + (-45)
						}, 1000, 'bounce');
					clicked.children('.views-field').children('.field-content').children('.faq-content').animate({
						height: height+10
					}, 1000, function(){
						var position = $(this).offset();
						var header = $(this).siblings('.faq-header').outerHeight();
						var text = $(this).children('.faq-content-inner').children('.section').outerHeight();
						var divOffset = header+text;
						$('body,html').animate({
							scrollTop: (position.top - divOffset - 100)
						}, 500);
					});
					clicked.addClass('active');
				}

				return false;
				});
			});
			
			$('document').ready(function(){
                //above is drupal template
                
                if ($('body').hasClass('front')) {
        
	                mobileMainImageFunction(); //call the mobileMainImageFunction function on document load
	
	                $(window).resize(function () {
	                	mobileMainImageFunction(); //call the mobileMainImageFunction function on resize
	                });
					
				}

				// Invitation 2014 - Checkist
				if ($("#checklist").length) {
				
					// Store checklist selection in cookies
					$('input[type="checkbox"]').change(function(){
						if($(this).is(":checked")) {
							$(this).parent(".inner").addClass("checked");
						} else {
							$(this).parent(".inner").removeClass("checked");
						}
					});
					
					var checkbox = $(".inner").find(':checkbox'), checklist = 'checked-item';
					
					checkbox.each(function() {
						$(this).attr('checked', $.cookie(checklist + '_' + $(this).attr('name')));
					});
					
					checkbox.click(function () {
						$.cookie(checklist + '_' + $(this).attr('name'), $(this).prop('checked'), { expires: 365 });
					});
					
					// Set height of rows
					$('.col').each(function(){
						var parentHeight = $(this).parent().height();
						$(this).height(parentHeight);    
					});
					
				}
				
				// YFS Trailer on click
				$(".video-embed").on('click', function(e) {
					var clicked = $(this);
					var overlay = clicked.parent().parent().find('.video-overlay');
					overlay.hide();
					var link = clicked.attr('data-link');
					var token = link.split('watch?v=');
					var embeddedVideo = '<iframe width="100%" height="100%" src="//www.youtube.com/embed/' + token[1] + '?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';
					clicked.replaceWith(embeddedVideo);
					videoResize('.ratio16-9');
                    videoResize('.ratio16-9 iframe');
        	e.preventDefault();
				});
				
        $(window).resize(function () {
        	videoResize('.ratio16-9');
            videoResize('.ratio16-9 iframe');
        });
				
		    function videoResize(element) {
		        var e = $(element);
		        e.each(function(){
		          var width = $(this).width();
		          var height = $(this).height();
		          var newHeight = ((width / 16 ) * 9);

		          $(this).height(newHeight);
		        });
		    }
				
                //replace the main image on the mobile //TODO CHECK THESE BREAKPOINTS ARE CORRECT (THEY ARE THE SAME USED IN THE CSS BUT THE JAVASCRIPT ONES SEEM TO BE DE AT DIFFERENT PLACES FOR SOME REASON - SEE THE OTHER WINDOW RESIZE FUNCTION)
                function mobileMainImageFunction() {
                  if ($(window).width() <= 480 && $('#Stage img') == true) {
                      src = $('#Stage img').attr("src");//.match(/[^\.]+/) + "homepageimagemobile.jpg";
                      $("#Stage img").attr("src", src.replace('ipadstaticQuestionmark', 'homepageimagemobile'));
                  } else if ($(window).width() >= 481 && $(window).width() <= 800  && $('#Stage img') == true) {
                      src = $('#Stage img').attr("src");//.match(/[^\.]+/) + "homepageimagemobile.jpg";
                      $("#Stage img").attr("src", src.replace('homepageimagemobile', 'ipadstaticQuestionmark'));
                   }
                }
              
              
              /*social sharing*/
              
              function sharePopups(clickClass, linkStart, dialogName) {
                $(document).on('click', clickClass, function(){
                    window.open(
                        linkStart+encodeURIComponent(location.href),
                        dialogName,
                        'width=626,height=600');
                    return false;
                });
              }
              
              sharePopups('.facebooksharebutton', 'https://www.facebook.com/sharer/sharer.php?u=', 'facebook-share-dialog');
              sharePopups('.twittersharebutton', 'https://twitter.com/share?url=', 'twitter-share-dialog');
              sharePopups('.pinterestsharebutton', 'http://pinterest.com/pin/create/button/?url=', 'pinterest-share-dialog');
              sharePopups('.googleplussharebutton', 'https://plus.google.com/share?url=', 'googleplus-share-dialog');


    		//TODO FIX THE JS STACKING ON THE JOURNAL PAGE (HOVER OFF AND ON TO THE IMAGES RAPIDLY)
    		
              /*This is for the journal page - displaying the descriptions*/
              $('.j-text-box').hover(function(){
                  //if ($(window).width() >= 1025) {
                  if (!$('html').hasClass('touch')) {
                      $(this).find('.article-description-span').slideToggle('slow');
                  }
              });

              //homepage
              $('.hp-text-box')
              .hover(function(){
                  // event.stopImmediatePropagation();
                  if (!$('html').hasClass('touch')) {
                  //if ($(window).width() >= 1025) {

                      $(this).find('.hp-text-box-hoverstate').stop(true).animate({
                          opacity:1
                      }, 400);

                      //moving up (old 800)
                      $(this).find('.hp-div').stop(true).animate({
                          marginBottom: "10px"
                      }, 400);

                      //fade in text (old 700, 100)
                      //$(this).find('.homepage-description-span').stop(true).show();
                      $(this).find('.homepage-description-span').stop(true).delay(350).animate({
                          opacity:1
                      }, 150);


                  }
              },
              function(){
                  // event.stopImmediatePropagation();
                  //if ($(window).width() >= 1025) {
					if (!$('html').hasClass('touch')) {
                      $(this).find('.hp-text-box-hoverstate').stop(true).animate({
                          opacity:0
                      }, 400);

                      $(this).find('.hp-div').stop(true).animate({
                          marginBottom: "0px"
                      }, 800);

                      //$(this).find('.homepage-description-span').stop(true).hide();
                      $(this).find('.homepage-description-span').stop(true).animate({
                          opacity:0
                      }, 1);
                    }
                });
                
                $('.box.advert-hover').hover(function() {
                   $(this).find('img.advert-hover-2').css('opacity', 1);
                },
                function() {
                    $(this).find('img.advert-hover-2').css('opacity', 0);
                });
                
                  var pluses = {};

                  var pluses = {};
    
                    $('#faqs-dropdown .faq-row').each(function(index, element) {
                        // Pluses
                        $(element).children('.faq-wrapper').children('.faq-content').children('.faq-question').append('<div class="change-plus" id="plus-'+index+'"></div>');
                        var plus = Raphael('plus-'+index, 60, 60);
                        pluses[index]  = plus.path("M 27 17 L 33 17 L 33 27 L 43 27 L 43 33 L 33 33 L 33 43 L 27 43 L 27 33 L 17 33 L 17 27 L 27 27Z M 27 17 Z");
                        var color = '#000';
                        pluses[index].attr("fill", color);
                        pluses[index].attr("stroke-width", 0);
        
                    });
      

                  $(".faq-question").click(function() {
                      var clicked = $(this).parent().parent().parent();

                      var wasActive = clicked.hasClass('active');
                      var previouslyActive = $('#faqs-dropdown .faq-row.active');

                      // Close any tabs that are open
                      if (previouslyActive.length) {
                          $(previouslyActive).children('.faq-wrapper').children('.faq-content').children('.faq-answer').animate({
                              height: 0
                          }, 1000);
                          $(previouslyActive).removeClass('active');
                          var index = $(previouslyActive).index();
                          pluses[index].animate({
                              transform: "r" + (0)
                              }, 1000);
                      }

                      // If it wasn't just open, open it
                      if (!wasActive) {
                          var height = $(clicked).children('.faq-wrapper').children('.faq-content').children('.faq-answer').children('.faq-answer-inner').height();

                          var index = $(clicked).index();
                          pluses[index].animate({
                              transform: "r" + (-45)
                              }, 1000, 'bounce');
                          clicked.children('.faq-wrapper').children('.faq-content').children('.faq-answer').animate({
                              height: height
                          }, 1000, function(){
                              var position = $(this).offset();
                              var header = $(this).siblings('.faq-question').outerHeight();
                              var text = $(this).children('.faq-answer-inner').outerHeight();
                              var divOffset = header+text;
                              $('body,html').animate({
                                  scrollTop: (position.top - 108)
                              }, 500);
                          });
                          clicked.addClass('active');
                      }

                      return false;
                  });

                  // Giving Video Replace on click 
                  
                  // Video Play
                  $('#giving-video a', context).click(function(){
                      $(this).parent().load("/loadvideo/giving");
                      return false;
                  });
                  
                  $('#giving-live-video a', context).click(function(){
                      $(this).parent().load("/loadvideo/giving-live");
                      return false;
                  });

				//below is drupal template
            });
        }
    };
})(jQuery);

/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// CommonJS
		factory(require('jquery'));
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}

	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}

	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}

	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		try {
			// Replace server-side written pluses with spaces.
			// If we can't decode the cookie, ignore it, it's unusable.
			// If we can't parse the cookie, ignore it, it's unusable.
			s = decodeURIComponent(s.replace(pluses, ' '));
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}

	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}

	var config = $.cookie = function (key, value, options) {

		// Write

		if (value !== undefined && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setTime(+t + days * 864e+5);
			}

			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// Read

		var result = key ? undefined : {};

		// To prevent the for loop in the first place assign an empty array
		// in case there are no cookies at all. Also prevents odd result when
		// calling $.cookie().
		var cookies = document.cookie ? document.cookie.split('; ') : [];

		for (var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			var name = decode(parts.shift());
			var cookie = parts.join('=');

			if (key && key === name) {
				// If second argument (value) is a function it's a converter...
				result = read(cookie, value);
				break;
			}

			// Prevent storing a cookie that we couldn't decode.
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		if ($.cookie(key) === undefined) {
			return false;
		}

		// Must not alter options, thus extending a fresh object...
		$.cookie(key, '', $.extend({}, options, { expires: -1 }));
		return !$.cookie(key);
	};

}));

/*
 CSS Browser Selector js v0.5.3 (July 2, 2013)

 -- original --
 Rafael Lima (http://rafael.adm.br)
 http://rafael.adm.br/css_browser_selector
 License: http://creativecommons.org/licenses/by/2.5/
 Contributors: http://rafael.adm.br/css_browser_selector#contributors
 -- /original --

 Fork project: http://code.google.com/p/css-browser-selector/
 Song Hyo-Jin (shj at xenosi.de)
 */
function css_browser_selector(n){var b=n.toLowerCase(),f=function(c){return b.indexOf(c)>-1},h="gecko",k="webkit",p="safari",j="chrome",d="opera",e="mobile",l=0,a=window.devicePixelRatio?(window.devicePixelRatio+"").replace(".","_"):"1";var i=[(!(/opera|webtv/.test(b))&&/msie\s(\d+)/.test(b)&&(l=RegExp.$1*1))?("ie ie"+l+((l==6||l==7)?" ie67 ie678 ie6789":(l==8)?" ie678 ie6789":(l==9)?" ie6789 ie9m":(l>9)?" ie9m":"")):(/trident\/\d+.*?;\s*rv:(\d+)\.(\d+)\)/.test(b)&&(l=[RegExp.$1,RegExp.$2]))?"ie ie"+l[0]+" ie"+l[0]+"_"+l[1]+" ie9m":(/firefox\/(\d+)\.(\d+)/.test(b)&&(re=RegExp))?h+" ff ff"+re.$1+" ff"+re.$1+"_"+re.$2:f("gecko/")?h:f(d)?d+(/version\/(\d+)/.test(b)?" "+d+RegExp.$1:(/opera(\s|\/)(\d+)/.test(b)?" "+d+RegExp.$2:"")):f("konqueror")?"konqueror":f("blackberry")?e+" blackberry":(f(j)||f("crios"))?k+" "+j:f("iron")?k+" iron":!f("cpu os")&&f("applewebkit/")?k+" "+p:f("mozilla/")?h:"",f("android")?e+" android":"",f("tablet")?"tablet":"",f("j2me")?e+" j2me":f("ipad; u; cpu os")?e+" chrome android tablet":f("ipad;u;cpu os")?e+" chromedef android tablet":f("iphone")?e+" ios iphone":f("ipod")?e+" ios ipod":f("ipad")?e+" ios ipad tablet":f("mac")?"mac":f("darwin")?"mac":f("webtv")?"webtv":f("win")?"win"+(f("windows nt 6.0")?" vista":""):f("freebsd")?"freebsd":(f("x11")||f("linux"))?"linux":"",(a!="1")?" retina ratio"+a:"","js portrait"].join(" ");if(window.jQuery&&!window.jQuery.browser){window.jQuery.browser=l?{msie:1,version:l}:{}}return i}(function(j,b){var c=css_browser_selector(navigator.userAgent);var g=j.documentElement;g.className+=" "+c;var a=c.replace(/^\s*|\s*$/g,"").split(/ +/);b.CSSBS=1;for(var f=0;f<a.length;f++){b["CSSBS_"+a[f]]=1}var e=function(d){return j.documentElement[d]||j.body[d]};if(b.jQuery){(function(q){var h="portrait",k="landscape";var i="smartnarrow",u="smartwide",x="tabletnarrow",r="tabletwide",w=i+" "+u+" "+x+" "+r+" pc";var v=q(g);var s=0,o=0;function d(){if(s!=0){return}try{var l=e("clientWidth"),p=e("clientHeight");if(l>p){v.removeClass(h).addClass(k)}else{v.removeClass(k).addClass(h)}if(l==o){return}o=l}catch(m){}s=setTimeout(n,100)}function n(){try{v.removeClass(w);v.addClass((o<=360)?i:(o<=640)?u:(o<=768)?x:(o<=1024)?r:"pc")}catch(l){}s=0}if(b.CSSBS_ie){setInterval(d,1000)}else{q(b).on("resize orientationchange",d).trigger("resize")}})(b.jQuery)}})(document,window);