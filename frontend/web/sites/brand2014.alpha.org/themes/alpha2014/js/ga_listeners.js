(function($) {

  // Switch between pages for specific events
  switch(window.location.pathname) {
    case '/try/results':

      // Get the search query (ostcode field)
      searchQuery = $('#edit-search-string').val();

      // Where there any results?
      resultCount = $('.result').size();
      if (resultCount > 0) {
        searchResults = 'result-found';
      } else {
        searchResults = 'no-result-found';
      }

      // Send the event to GA
      ga("send", {
        "hitType": "event",
        "eventCategory": "Try Alpha search",
        "eventAction": searchResults,
        "eventLabel": searchQuery,
        "nonInteraction": 1
      });
      break;
  }

})(jQuery);